; ModuleID = 'psroi_golden.c'
source_filename = "psroi_golden.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32-S128"
target triple = "riscv32"

@.str = private unnamed_addr constant [18 x i8] c"failed, %d != %d\0A\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"passed\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define dso_local i32 @bilinear_interpolate(ptr noundef %0, i16 noundef signext %1, i16 noundef signext %2, i16 noundef signext %3, i16 noundef signext %4) #0 !dbg !8 {
  %6 = alloca i32, align 4
  %7 = alloca ptr, align 4
  %8 = alloca i16, align 2
  %9 = alloca i16, align 2
  %10 = alloca i16, align 2
  %11 = alloca i16, align 2
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca i16, align 2
  %21 = alloca i16, align 2
  %22 = alloca i16, align 2
  %23 = alloca i16, align 2
  %24 = alloca i16, align 2
  %25 = alloca i16, align 2
  %26 = alloca i16, align 2
  %27 = alloca i16, align 2
  %28 = alloca i16, align 2
  store ptr %0, ptr %7, align 4
  store i16 %1, ptr %8, align 2
  store i16 %2, ptr %9, align 2
  store i16 %3, ptr %10, align 2
  store i16 %4, ptr %11, align 2
  store i32 0, ptr %12, align 4, !dbg !11
  %29 = load i16, ptr %10, align 2, !dbg !12
  %30 = sext i16 %29 to i32, !dbg !12
  %31 = ashr i32 %30, 4, !dbg !13
  %32 = load i16, ptr %8, align 2, !dbg !14
  %33 = sext i16 %32 to i32, !dbg !14
  %34 = icmp sge i32 %31, %33, !dbg !15
  br i1 %34, label %42, label %35, !dbg !16

35:                                               ; preds = %5
  %36 = load i16, ptr %11, align 2, !dbg !17
  %37 = sext i16 %36 to i32, !dbg !17
  %38 = ashr i32 %37, 4, !dbg !18
  %39 = load i16, ptr %9, align 2, !dbg !19
  %40 = sext i16 %39 to i32, !dbg !19
  %41 = icmp sge i32 %38, %40, !dbg !20
  br i1 %41, label %42, label %43, !dbg !21

42:                                               ; preds = %35, %5
  store i32 0, ptr %6, align 4, !dbg !22
  br label %205, !dbg !22

43:                                               ; preds = %35
  %44 = load i16, ptr %10, align 2, !dbg !23
  %45 = sext i16 %44 to i32, !dbg !23
  %46 = icmp sle i32 %45, 0, !dbg !24
  br i1 %46, label %47, label %48, !dbg !23

47:                                               ; preds = %43
  store i16 0, ptr %10, align 2, !dbg !25
  br label %48, !dbg !26

48:                                               ; preds = %47, %43
  %49 = load i16, ptr %11, align 2, !dbg !27
  %50 = sext i16 %49 to i32, !dbg !27
  %51 = icmp sle i32 %50, 0, !dbg !28
  br i1 %51, label %52, label %53, !dbg !27

52:                                               ; preds = %48
  store i16 0, ptr %11, align 2, !dbg !29
  br label %53, !dbg !30

53:                                               ; preds = %52, %48
  %54 = load i16, ptr %10, align 2, !dbg !31
  %55 = sext i16 %54 to i32, !dbg !31
  %56 = ashr i32 %55, 4, !dbg !32
  store i32 %56, ptr %13, align 4, !dbg !33
  %57 = load i16, ptr %11, align 2, !dbg !34
  %58 = sext i16 %57 to i32, !dbg !34
  %59 = ashr i32 %58, 4, !dbg !35
  store i32 %59, ptr %14, align 4, !dbg !36
  %60 = load i32, ptr %13, align 4, !dbg !37
  %61 = load i16, ptr %8, align 2, !dbg !38
  %62 = sext i16 %61 to i32, !dbg !38
  %63 = sub nsw i32 %62, 1, !dbg !39
  %64 = icmp sge i32 %60, %63, !dbg !40
  br i1 %64, label %65, label %72, !dbg !37

65:                                               ; preds = %53
  %66 = load i16, ptr %8, align 2, !dbg !41
  %67 = sext i16 %66 to i32, !dbg !41
  %68 = sub nsw i32 %67, 1, !dbg !42
  store i32 %68, ptr %13, align 4, !dbg !43
  store i32 %68, ptr %15, align 4, !dbg !44
  %69 = load i32, ptr %13, align 4, !dbg !45
  %70 = shl i32 %69, 4, !dbg !46
  %71 = trunc i32 %70 to i16, !dbg !45
  store i16 %71, ptr %10, align 2, !dbg !47
  br label %75, !dbg !48

72:                                               ; preds = %53
  %73 = load i32, ptr %13, align 4, !dbg !49
  %74 = add nsw i32 %73, 1, !dbg !50
  store i32 %74, ptr %15, align 4, !dbg !51
  br label %75

75:                                               ; preds = %72, %65
  %76 = load i32, ptr %14, align 4, !dbg !52
  %77 = load i16, ptr %9, align 2, !dbg !53
  %78 = sext i16 %77 to i32, !dbg !53
  %79 = sub nsw i32 %78, 1, !dbg !54
  %80 = icmp sge i32 %76, %79, !dbg !55
  br i1 %80, label %81, label %88, !dbg !52

81:                                               ; preds = %75
  %82 = load i16, ptr %9, align 2, !dbg !56
  %83 = sext i16 %82 to i32, !dbg !56
  %84 = sub nsw i32 %83, 1, !dbg !57
  store i32 %84, ptr %14, align 4, !dbg !58
  store i32 %84, ptr %16, align 4, !dbg !59
  %85 = load i32, ptr %14, align 4, !dbg !60
  %86 = shl i32 %85, 4, !dbg !61
  %87 = trunc i32 %86 to i16, !dbg !60
  store i16 %87, ptr %11, align 2, !dbg !62
  br label %91, !dbg !63

88:                                               ; preds = %75
  %89 = load i32, ptr %14, align 4, !dbg !64
  %90 = add nsw i32 %89, 1, !dbg !65
  store i32 %90, ptr %16, align 4, !dbg !66
  br label %91

91:                                               ; preds = %88, %81
  %92 = load i16, ptr %10, align 2, !dbg !67
  %93 = sext i16 %92 to i32, !dbg !67
  %94 = load i32, ptr %13, align 4, !dbg !68
  %95 = shl i32 %94, 4, !dbg !69
  %96 = sub nsw i32 %93, %95, !dbg !70
  %97 = trunc i32 %96 to i16, !dbg !67
  store i16 %97, ptr %17, align 2, !dbg !71
  %98 = load i16, ptr %11, align 2, !dbg !72
  %99 = sext i16 %98 to i32, !dbg !72
  %100 = load i32, ptr %14, align 4, !dbg !73
  %101 = shl i32 %100, 4, !dbg !74
  %102 = sub nsw i32 %99, %101, !dbg !75
  %103 = trunc i32 %102 to i16, !dbg !72
  store i16 %103, ptr %18, align 2, !dbg !76
  %104 = load i16, ptr %17, align 2, !dbg !77
  %105 = zext i16 %104 to i32, !dbg !77
  %106 = sub nsw i32 16, %105, !dbg !78
  %107 = trunc i32 %106 to i16, !dbg !79
  store i16 %107, ptr %19, align 2, !dbg !80
  %108 = load i16, ptr %18, align 2, !dbg !81
  %109 = zext i16 %108 to i32, !dbg !81
  %110 = sub nsw i32 16, %109, !dbg !82
  %111 = trunc i32 %110 to i16, !dbg !83
  store i16 %111, ptr %20, align 2, !dbg !84
  %112 = load ptr, ptr %7, align 4, !dbg !85
  %113 = load i32, ptr %13, align 4, !dbg !86
  %114 = load i16, ptr %9, align 2, !dbg !87
  %115 = sext i16 %114 to i32, !dbg !87
  %116 = mul nsw i32 %113, %115, !dbg !88
  %117 = load i32, ptr %14, align 4, !dbg !89
  %118 = add nsw i32 %116, %117, !dbg !90
  %119 = getelementptr inbounds i8, ptr %112, i32 %118, !dbg !85
  %120 = load i8, ptr %119, align 1, !dbg !85
  %121 = zext i8 %120 to i16, !dbg !85
  store i16 %121, ptr %21, align 2, !dbg !91
  %122 = load ptr, ptr %7, align 4, !dbg !92
  %123 = load i32, ptr %13, align 4, !dbg !93
  %124 = load i16, ptr %9, align 2, !dbg !94
  %125 = sext i16 %124 to i32, !dbg !94
  %126 = mul nsw i32 %123, %125, !dbg !95
  %127 = load i32, ptr %16, align 4, !dbg !96
  %128 = add nsw i32 %126, %127, !dbg !97
  %129 = getelementptr inbounds i8, ptr %122, i32 %128, !dbg !92
  %130 = load i8, ptr %129, align 1, !dbg !92
  %131 = zext i8 %130 to i16, !dbg !92
  store i16 %131, ptr %22, align 2, !dbg !98
  %132 = load ptr, ptr %7, align 4, !dbg !99
  %133 = load i32, ptr %15, align 4, !dbg !100
  %134 = load i16, ptr %9, align 2, !dbg !101
  %135 = sext i16 %134 to i32, !dbg !101
  %136 = mul nsw i32 %133, %135, !dbg !102
  %137 = load i32, ptr %14, align 4, !dbg !103
  %138 = add nsw i32 %136, %137, !dbg !104
  %139 = getelementptr inbounds i8, ptr %132, i32 %138, !dbg !99
  %140 = load i8, ptr %139, align 1, !dbg !99
  %141 = zext i8 %140 to i16, !dbg !99
  store i16 %141, ptr %23, align 2, !dbg !105
  %142 = load ptr, ptr %7, align 4, !dbg !106
  %143 = load i32, ptr %15, align 4, !dbg !107
  %144 = load i16, ptr %9, align 2, !dbg !108
  %145 = sext i16 %144 to i32, !dbg !108
  %146 = mul nsw i32 %143, %145, !dbg !109
  %147 = load i32, ptr %16, align 4, !dbg !110
  %148 = add nsw i32 %146, %147, !dbg !111
  %149 = getelementptr inbounds i8, ptr %142, i32 %148, !dbg !106
  %150 = load i8, ptr %149, align 1, !dbg !106
  %151 = zext i8 %150 to i16, !dbg !106
  store i16 %151, ptr %24, align 2, !dbg !112
  %152 = load i16, ptr %19, align 2, !dbg !113
  %153 = zext i16 %152 to i32, !dbg !113
  %154 = load i16, ptr %20, align 2, !dbg !114
  %155 = zext i16 %154 to i32, !dbg !114
  %156 = mul nsw i32 %153, %155, !dbg !115
  %157 = ashr i32 %156, 4, !dbg !116
  %158 = trunc i32 %157 to i16, !dbg !117
  store i16 %158, ptr %25, align 2, !dbg !118
  %159 = load i16, ptr %19, align 2, !dbg !119
  %160 = zext i16 %159 to i32, !dbg !119
  %161 = load i16, ptr %18, align 2, !dbg !120
  %162 = zext i16 %161 to i32, !dbg !120
  %163 = mul nsw i32 %160, %162, !dbg !121
  %164 = ashr i32 %163, 4, !dbg !122
  %165 = trunc i32 %164 to i16, !dbg !123
  store i16 %165, ptr %26, align 2, !dbg !124
  %166 = load i16, ptr %17, align 2, !dbg !125
  %167 = zext i16 %166 to i32, !dbg !125
  %168 = load i16, ptr %20, align 2, !dbg !126
  %169 = zext i16 %168 to i32, !dbg !126
  %170 = mul nsw i32 %167, %169, !dbg !127
  %171 = ashr i32 %170, 4, !dbg !128
  %172 = trunc i32 %171 to i16, !dbg !129
  store i16 %172, ptr %27, align 2, !dbg !130
  %173 = load i16, ptr %17, align 2, !dbg !131
  %174 = zext i16 %173 to i32, !dbg !131
  %175 = load i16, ptr %18, align 2, !dbg !132
  %176 = zext i16 %175 to i32, !dbg !132
  %177 = mul nsw i32 %174, %176, !dbg !133
  %178 = ashr i32 %177, 4, !dbg !134
  %179 = trunc i32 %178 to i16, !dbg !135
  store i16 %179, ptr %28, align 2, !dbg !136
  %180 = load i16, ptr %25, align 2, !dbg !137
  %181 = zext i16 %180 to i32, !dbg !137
  %182 = load i16, ptr %21, align 2, !dbg !138
  %183 = zext i16 %182 to i32, !dbg !138
  %184 = mul nsw i32 %181, %183, !dbg !139
  %185 = load i16, ptr %26, align 2, !dbg !140
  %186 = zext i16 %185 to i32, !dbg !140
  %187 = load i16, ptr %22, align 2, !dbg !141
  %188 = zext i16 %187 to i32, !dbg !141
  %189 = mul nsw i32 %186, %188, !dbg !142
  %190 = add nsw i32 %184, %189, !dbg !143
  %191 = load i16, ptr %27, align 2, !dbg !144
  %192 = zext i16 %191 to i32, !dbg !144
  %193 = load i16, ptr %23, align 2, !dbg !145
  %194 = zext i16 %193 to i32, !dbg !145
  %195 = mul nsw i32 %192, %194, !dbg !146
  %196 = add nsw i32 %190, %195, !dbg !147
  %197 = load i16, ptr %28, align 2, !dbg !148
  %198 = zext i16 %197 to i32, !dbg !148
  %199 = load i16, ptr %24, align 2, !dbg !149
  %200 = zext i16 %199 to i32, !dbg !149
  %201 = mul nsw i32 %198, %200, !dbg !150
  %202 = add nsw i32 %196, %201, !dbg !151
  %203 = lshr i32 %202, 4, !dbg !152
  store i32 %203, ptr %12, align 4, !dbg !153
  %204 = load i32, ptr %12, align 4, !dbg !154
  store i32 %204, ptr %6, align 4, !dbg !155
  br label %205, !dbg !155

205:                                              ; preds = %91, %42
  %206 = load i32, ptr %6, align 4, !dbg !156
  ret i32 %206, !dbg !156
}

; Function Attrs: noinline nounwind optnone
define dso_local void @psroi_align_golden(ptr noundef %0, ptr noundef %1, ptr noundef %2, i32 noundef %3, i32 noundef %4, i32 noundef %5, i32 noundef %6, i32 noundef %7, i32 noundef %8, i32 noundef %9, i32 noundef %10, i32 noundef %11, i8 noundef %12) #0 !dbg !157 {
  %14 = alloca ptr, align 4
  %15 = alloca ptr, align 4
  %16 = alloca ptr, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca i8, align 1
  %27 = alloca ptr, align 4
  %28 = alloca i16, align 2
  %29 = alloca i16, align 2
  %30 = alloca i32, align 4
  %31 = alloca i16, align 2
  %32 = alloca i16, align 2
  %33 = alloca i16, align 2
  %34 = alloca i16, align 2
  %35 = alloca i16, align 2
  %36 = alloca i16, align 2
  %37 = alloca i16, align 2
  %38 = alloca i16, align 2
  %39 = alloca i16, align 2
  %40 = alloca i16, align 2
  %41 = alloca i32, align 4
  %42 = alloca i32, align 4
  %43 = alloca i16, align 2
  %44 = alloca i16, align 2
  %45 = alloca i32, align 4
  %46 = alloca i32, align 4
  %47 = alloca i32, align 4
  %48 = alloca i32, align 4
  %49 = alloca i16, align 2
  %50 = alloca i32, align 4
  %51 = alloca i16, align 2
  store ptr %0, ptr %14, align 4
  store ptr %1, ptr %15, align 4
  store ptr %2, ptr %16, align 4
  store i32 %3, ptr %17, align 4
  store i32 %4, ptr %18, align 4
  store i32 %5, ptr %19, align 4
  store i32 %6, ptr %20, align 4
  store i32 %7, ptr %21, align 4
  store i32 %8, ptr %22, align 4
  store i32 %9, ptr %23, align 4
  store i32 %10, ptr %24, align 4
  store i32 %11, ptr %25, align 4
  store i8 %12, ptr %26, align 1
  %52 = load ptr, ptr %14, align 4, !dbg !158
  store ptr %52, ptr %27, align 4, !dbg !159
  %53 = load i32, ptr %18, align 4, !dbg !160
  %54 = load i32, ptr %20, align 4, !dbg !161
  %55 = udiv i32 %53, %54, !dbg !162
  %56 = trunc i32 %55 to i16, !dbg !160
  store i16 %56, ptr %28, align 2, !dbg !163
  %57 = load i32, ptr %18, align 4, !dbg !164
  %58 = load i32, ptr %20, align 4, !dbg !165
  %59 = urem i32 %57, %58, !dbg !166
  %60 = trunc i32 %59 to i16, !dbg !164
  store i16 %60, ptr %29, align 2, !dbg !167
  store i32 0, ptr %30, align 4, !dbg !168
  br label %61, !dbg !169

61:                                               ; preds = %259, %13
  %62 = load i32, ptr %30, align 4, !dbg !170
  %63 = load i32, ptr %17, align 4, !dbg !171
  %64 = icmp ult i32 %62, %63, !dbg !172
  br i1 %64, label %65, label %262, !dbg !173

65:                                               ; preds = %61
  %66 = load ptr, ptr %14, align 4, !dbg !174
  store ptr %66, ptr %27, align 4, !dbg !175
  %67 = load ptr, ptr %15, align 4, !dbg !176
  %68 = load i32, ptr %30, align 4, !dbg !177
  %69 = mul i32 4, %68, !dbg !178
  %70 = getelementptr inbounds i16, ptr %67, i32 %69, !dbg !176
  %71 = load i16, ptr %70, align 2, !dbg !176
  store i16 %71, ptr %31, align 2, !dbg !179
  %72 = load ptr, ptr %15, align 4, !dbg !180
  %73 = load i32, ptr %30, align 4, !dbg !181
  %74 = mul i32 4, %73, !dbg !182
  %75 = add i32 %74, 1, !dbg !183
  %76 = getelementptr inbounds i16, ptr %72, i32 %75, !dbg !180
  %77 = load i16, ptr %76, align 2, !dbg !180
  store i16 %77, ptr %32, align 2, !dbg !184
  %78 = load ptr, ptr %15, align 4, !dbg !185
  %79 = load i32, ptr %30, align 4, !dbg !186
  %80 = mul i32 4, %79, !dbg !187
  %81 = add i32 %80, 2, !dbg !188
  %82 = getelementptr inbounds i16, ptr %78, i32 %81, !dbg !185
  %83 = load i16, ptr %82, align 2, !dbg !185
  store i16 %83, ptr %33, align 2, !dbg !189
  %84 = load ptr, ptr %15, align 4, !dbg !190
  %85 = load i32, ptr %30, align 4, !dbg !191
  %86 = mul i32 4, %85, !dbg !192
  %87 = add i32 %86, 3, !dbg !193
  %88 = getelementptr inbounds i16, ptr %84, i32 %87, !dbg !190
  %89 = load i16, ptr %88, align 2, !dbg !190
  store i16 %89, ptr %34, align 2, !dbg !194
  %90 = load i16, ptr %33, align 2, !dbg !195
  %91 = zext i16 %90 to i32, !dbg !195
  %92 = load i16, ptr %31, align 2, !dbg !195
  %93 = zext i16 %92 to i32, !dbg !195
  %94 = sub nsw i32 %91, %93, !dbg !195
  %95 = icmp sgt i32 %94, 16, !dbg !195
  br i1 %95, label %96, label %102, !dbg !195

96:                                               ; preds = %65
  %97 = load i16, ptr %33, align 2, !dbg !195
  %98 = zext i16 %97 to i32, !dbg !195
  %99 = load i16, ptr %31, align 2, !dbg !195
  %100 = zext i16 %99 to i32, !dbg !195
  %101 = sub nsw i32 %98, %100, !dbg !195
  br label %103, !dbg !195

102:                                              ; preds = %65
  br label %103, !dbg !195

103:                                              ; preds = %102, %96
  %104 = phi i32 [ %101, %96 ], [ 16, %102 ], !dbg !195
  %105 = trunc i32 %104 to i16, !dbg !195
  store i16 %105, ptr %35, align 2, !dbg !196
  %106 = load i16, ptr %34, align 2, !dbg !197
  %107 = zext i16 %106 to i32, !dbg !197
  %108 = load i16, ptr %32, align 2, !dbg !197
  %109 = zext i16 %108 to i32, !dbg !197
  %110 = sub nsw i32 %107, %109, !dbg !197
  %111 = icmp sgt i32 %110, 16, !dbg !197
  br i1 %111, label %112, label %118, !dbg !197

112:                                              ; preds = %103
  %113 = load i16, ptr %34, align 2, !dbg !197
  %114 = zext i16 %113 to i32, !dbg !197
  %115 = load i16, ptr %32, align 2, !dbg !197
  %116 = zext i16 %115 to i32, !dbg !197
  %117 = sub nsw i32 %114, %116, !dbg !197
  br label %119, !dbg !197

118:                                              ; preds = %103
  br label %119, !dbg !197

119:                                              ; preds = %118, %112
  %120 = phi i32 [ %117, %112 ], [ 16, %118 ], !dbg !197
  %121 = trunc i32 %120 to i16, !dbg !197
  store i16 %121, ptr %36, align 2, !dbg !198
  %122 = load i32, ptr %20, align 4, !dbg !199
  %123 = uitofp i32 %122 to double, !dbg !199
  %124 = fdiv double 1.000000e+00, %123, !dbg !200
  %125 = fmul double %124, 2.560000e+02, !dbg !201
  %126 = fptoui double %125 to i16, !dbg !202
  store i16 %126, ptr %37, align 2, !dbg !203
  %127 = load i32, ptr %19, align 4, !dbg !204
  %128 = uitofp i32 %127 to double, !dbg !204
  %129 = fdiv double 1.000000e+00, %128, !dbg !205
  %130 = fmul double %129, 2.560000e+02, !dbg !206
  %131 = fptoui double %130 to i16, !dbg !207
  store i16 %131, ptr %38, align 2, !dbg !208
  %132 = load i16, ptr %35, align 2, !dbg !209
  %133 = zext i16 %132 to i32, !dbg !209
  %134 = load i16, ptr %37, align 2, !dbg !210
  %135 = zext i16 %134 to i32, !dbg !210
  %136 = mul nsw i32 %133, %135, !dbg !211
  %137 = ashr i32 %136, 8, !dbg !212
  %138 = trunc i32 %137 to i16, !dbg !213
  store i16 %138, ptr %39, align 2, !dbg !214
  %139 = load i16, ptr %36, align 2, !dbg !215
  %140 = zext i16 %139 to i32, !dbg !215
  %141 = load i16, ptr %38, align 2, !dbg !216
  %142 = zext i16 %141 to i32, !dbg !216
  %143 = mul nsw i32 %140, %142, !dbg !217
  %144 = ashr i32 %143, 8, !dbg !218
  %145 = trunc i32 %144 to i16, !dbg !219
  store i16 %145, ptr %40, align 2, !dbg !220
  %146 = load i8, ptr %26, align 1, !dbg !221
  %147 = zext i8 %146 to i32, !dbg !221
  store i32 %147, ptr %41, align 4, !dbg !222
  %148 = load i8, ptr %26, align 1, !dbg !223
  %149 = zext i8 %148 to i32, !dbg !223
  store i32 %149, ptr %42, align 4, !dbg !224
  %150 = load i16, ptr %39, align 2, !dbg !225
  %151 = zext i16 %150 to i32, !dbg !225
  %152 = load i8, ptr %26, align 1, !dbg !226
  %153 = zext i8 %152 to i32, !dbg !226
  %154 = mul nsw i32 %153, 2, !dbg !227
  %155 = sdiv i32 %151, %154, !dbg !228
  %156 = trunc i32 %155 to i16, !dbg !225
  store i16 %156, ptr %43, align 2, !dbg !229
  %157 = load i16, ptr %40, align 2, !dbg !230
  %158 = zext i16 %157 to i32, !dbg !230
  %159 = load i8, ptr %26, align 1, !dbg !231
  %160 = zext i8 %159 to i32, !dbg !231
  %161 = mul nsw i32 %160, 2, !dbg !232
  %162 = sdiv i32 %158, %161, !dbg !233
  %163 = trunc i32 %162 to i16, !dbg !230
  store i16 %163, ptr %44, align 2, !dbg !234
  store i32 0, ptr %45, align 4, !dbg !235
  br label %164, !dbg !236

164:                                              ; preds = %255, %119
  %165 = load i32, ptr %45, align 4, !dbg !237
  %166 = load i32, ptr %21, align 4, !dbg !238
  %167 = icmp ult i32 %165, %166, !dbg !239
  br i1 %167, label %168, label %258, !dbg !240

168:                                              ; preds = %164
  store i32 0, ptr %46, align 4, !dbg !241
  %169 = load i32, ptr %30, align 4, !dbg !242
  %170 = load i32, ptr %24, align 4, !dbg !243
  %171 = mul i32 %169, %170, !dbg !244
  %172 = load i32, ptr %45, align 4, !dbg !245
  %173 = load i32, ptr %25, align 4, !dbg !246
  %174 = mul i32 %172, %173, !dbg !247
  %175 = add i32 %171, %174, !dbg !248
  %176 = load i16, ptr %28, align 2, !dbg !249
  %177 = zext i16 %176 to i32, !dbg !249
  %178 = load i32, ptr %20, align 4, !dbg !250
  %179 = mul i32 %177, %178, !dbg !251
  %180 = add i32 %175, %179, !dbg !252
  %181 = load i16, ptr %29, align 2, !dbg !253
  %182 = zext i16 %181 to i32, !dbg !253
  %183 = add i32 %180, %182, !dbg !254
  store i32 %183, ptr %47, align 4, !dbg !255
  store i32 0, ptr %48, align 4, !dbg !256
  br label %184, !dbg !257

184:                                              ; preds = %240, %168
  %185 = load i32, ptr %48, align 4, !dbg !258
  %186 = load i32, ptr %42, align 4, !dbg !259
  %187 = icmp slt i32 %185, %186, !dbg !260
  br i1 %187, label %188, label %243, !dbg !261

188:                                              ; preds = %184
  %189 = load i16, ptr %32, align 2, !dbg !262
  %190 = zext i16 %189 to i32, !dbg !262
  %191 = load i16, ptr %28, align 2, !dbg !263
  %192 = zext i16 %191 to i32, !dbg !263
  %193 = load i16, ptr %40, align 2, !dbg !264
  %194 = zext i16 %193 to i32, !dbg !264
  %195 = mul nsw i32 %192, %194, !dbg !265
  %196 = add nsw i32 %190, %195, !dbg !266
  %197 = load i32, ptr %48, align 4, !dbg !267
  %198 = mul nsw i32 2, %197, !dbg !268
  %199 = add nsw i32 %198, 1, !dbg !269
  %200 = load i16, ptr %44, align 2, !dbg !270
  %201 = zext i16 %200 to i32, !dbg !270
  %202 = mul nsw i32 %199, %201, !dbg !271
  %203 = add nsw i32 %196, %202, !dbg !272
  %204 = trunc i32 %203 to i16, !dbg !262
  store i16 %204, ptr %49, align 2, !dbg !273
  store i32 0, ptr %50, align 4, !dbg !274
  br label %205, !dbg !275

205:                                              ; preds = %236, %188
  %206 = load i32, ptr %50, align 4, !dbg !276
  %207 = load i32, ptr %41, align 4, !dbg !277
  %208 = icmp slt i32 %206, %207, !dbg !278
  br i1 %208, label %209, label %239, !dbg !279

209:                                              ; preds = %205
  %210 = load i16, ptr %31, align 2, !dbg !280
  %211 = zext i16 %210 to i32, !dbg !280
  %212 = load i16, ptr %29, align 2, !dbg !281
  %213 = zext i16 %212 to i32, !dbg !281
  %214 = load i16, ptr %39, align 2, !dbg !282
  %215 = zext i16 %214 to i32, !dbg !282
  %216 = mul nsw i32 %213, %215, !dbg !283
  %217 = add nsw i32 %211, %216, !dbg !284
  %218 = load i32, ptr %50, align 4, !dbg !285
  %219 = mul nsw i32 2, %218, !dbg !286
  %220 = add nsw i32 %219, 1, !dbg !287
  %221 = load i16, ptr %43, align 2, !dbg !288
  %222 = zext i16 %221 to i32, !dbg !288
  %223 = mul nsw i32 %220, %222, !dbg !289
  %224 = add nsw i32 %217, %223, !dbg !290
  %225 = trunc i32 %224 to i16, !dbg !280
  store i16 %225, ptr %51, align 2, !dbg !291
  %226 = load ptr, ptr %27, align 4, !dbg !292
  %227 = load i32, ptr %22, align 4, !dbg !293
  %228 = trunc i32 %227 to i16, !dbg !293
  %229 = load i32, ptr %23, align 4, !dbg !294
  %230 = trunc i32 %229 to i16, !dbg !294
  %231 = load i16, ptr %49, align 2, !dbg !295
  %232 = load i16, ptr %51, align 2, !dbg !296
  %233 = call i32 @bilinear_interpolate(ptr noundef %226, i16 noundef signext %228, i16 noundef signext %230, i16 noundef signext %231, i16 noundef signext %232), !dbg !297
  %234 = load i32, ptr %46, align 4, !dbg !298
  %235 = add i32 %234, %233, !dbg !298
  store i32 %235, ptr %46, align 4, !dbg !298
  br label %236, !dbg !299

236:                                              ; preds = %209
  %237 = load i32, ptr %50, align 4, !dbg !300
  %238 = add nsw i32 %237, 1, !dbg !300
  store i32 %238, ptr %50, align 4, !dbg !300
  br label %205, !dbg !279, !llvm.loop !301

239:                                              ; preds = %205
  br label %240, !dbg !303

240:                                              ; preds = %239
  %241 = load i32, ptr %48, align 4, !dbg !304
  %242 = add nsw i32 %241, 1, !dbg !304
  store i32 %242, ptr %48, align 4, !dbg !304
  br label %184, !dbg !261, !llvm.loop !305

243:                                              ; preds = %184
  %244 = load i32, ptr %46, align 4, !dbg !306
  %245 = lshr i32 %244, 2, !dbg !307
  %246 = trunc i32 %245 to i16, !dbg !308
  %247 = load ptr, ptr %16, align 4, !dbg !309
  %248 = load i32, ptr %47, align 4, !dbg !310
  %249 = getelementptr inbounds i16, ptr %247, i32 %248, !dbg !309
  store i16 %246, ptr %249, align 2, !dbg !311
  %250 = load i32, ptr %22, align 4, !dbg !312
  %251 = load i32, ptr %23, align 4, !dbg !313
  %252 = mul i32 %250, %251, !dbg !314
  %253 = load ptr, ptr %27, align 4, !dbg !315
  %254 = getelementptr inbounds i8, ptr %253, i32 %252, !dbg !315
  store ptr %254, ptr %27, align 4, !dbg !315
  br label %255, !dbg !316

255:                                              ; preds = %243
  %256 = load i32, ptr %45, align 4, !dbg !317
  %257 = add nsw i32 %256, 1, !dbg !317
  store i32 %257, ptr %45, align 4, !dbg !317
  br label %164, !dbg !240, !llvm.loop !318

258:                                              ; preds = %164
  br label %259, !dbg !319

259:                                              ; preds = %258
  %260 = load i32, ptr %30, align 4, !dbg !320
  %261 = add i32 %260, 1, !dbg !320
  store i32 %261, ptr %30, align 4, !dbg !320
  br label %61, !dbg !173, !llvm.loop !321

262:                                              ; preds = %61
  ret void, !dbg !322
}

; Function Attrs: noinline nounwind optnone
define dso_local void @psroi_pooling_align_golden(ptr noundef %0, ptr noundef %1, ptr noundef %2, i32 noundef %3, i32 noundef %4, i32 noundef %5, i32 noundef %6, i32 noundef %7, i32 noundef %8, i32 noundef %9, i32 noundef %10, i32 noundef %11, i8 noundef %12) #0 !dbg !323 {
  %14 = alloca ptr, align 4
  %15 = alloca ptr, align 4
  %16 = alloca ptr, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca i8, align 1
  %27 = alloca ptr, align 4
  %28 = alloca i32, align 4
  store ptr %0, ptr %14, align 4
  store ptr %1, ptr %15, align 4
  store ptr %2, ptr %16, align 4
  store i32 %3, ptr %17, align 4
  store i32 %4, ptr %18, align 4
  store i32 %5, ptr %19, align 4
  store i32 %6, ptr %20, align 4
  store i32 %7, ptr %21, align 4
  store i32 %8, ptr %22, align 4
  store i32 %9, ptr %23, align 4
  store i32 %10, ptr %24, align 4
  store i32 %11, ptr %25, align 4
  store i8 %12, ptr %26, align 1
  %29 = load ptr, ptr %14, align 4, !dbg !324
  store ptr %29, ptr %27, align 4, !dbg !325
  store i32 0, ptr %28, align 4, !dbg !326
  br label %30, !dbg !327

30:                                               ; preds = %55, %13
  %31 = load i32, ptr %28, align 4, !dbg !328
  %32 = load i32, ptr %18, align 4, !dbg !329
  %33 = icmp ult i32 %31, %32, !dbg !330
  br i1 %33, label %34, label %58, !dbg !331

34:                                               ; preds = %30
  %35 = load ptr, ptr %27, align 4, !dbg !332
  %36 = load ptr, ptr %15, align 4, !dbg !333
  %37 = load ptr, ptr %16, align 4, !dbg !334
  %38 = load i32, ptr %17, align 4, !dbg !335
  %39 = load i32, ptr %28, align 4, !dbg !336
  %40 = load i32, ptr %19, align 4, !dbg !337
  %41 = load i32, ptr %20, align 4, !dbg !338
  %42 = load i32, ptr %21, align 4, !dbg !339
  %43 = load i32, ptr %22, align 4, !dbg !340
  %44 = load i32, ptr %23, align 4, !dbg !341
  %45 = load i32, ptr %24, align 4, !dbg !342
  %46 = load i32, ptr %25, align 4, !dbg !343
  %47 = load i8, ptr %26, align 1, !dbg !344
  call void @psroi_align_golden(ptr noundef %35, ptr noundef %36, ptr noundef %37, i32 noundef %38, i32 noundef %39, i32 noundef %40, i32 noundef %41, i32 noundef %42, i32 noundef %43, i32 noundef %44, i32 noundef %45, i32 noundef %46, i8 noundef %47), !dbg !345
  %48 = load i32, ptr %22, align 4, !dbg !346
  %49 = load i32, ptr %23, align 4, !dbg !347
  %50 = mul i32 %48, %49, !dbg !348
  %51 = load i32, ptr %21, align 4, !dbg !349
  %52 = mul i32 %50, %51, !dbg !350
  %53 = load ptr, ptr %27, align 4, !dbg !351
  %54 = getelementptr inbounds i8, ptr %53, i32 %52, !dbg !351
  store ptr %54, ptr %27, align 4, !dbg !351
  br label %55, !dbg !352

55:                                               ; preds = %34
  %56 = load i32, ptr %28, align 4, !dbg !353
  %57 = add i32 %56, 1, !dbg !353
  store i32 %57, ptr %28, align 4, !dbg !353
  br label %30, !dbg !331, !llvm.loop !354

58:                                               ; preds = %30
  ret void, !dbg !355
}

; Function Attrs: noinline nounwind optnone
define dso_local i32 @main() #0 !dbg !356 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca ptr, align 4
  %20 = alloca i32, align 4
  %21 = alloca i32, align 4
  %22 = alloca i32, align 4
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca i32, align 4
  %27 = alloca i32, align 4
  %28 = alloca i32, align 4
  %29 = alloca i32, align 4
  %30 = alloca i32, align 4
  %31 = alloca i32, align 4
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca i32, align 4
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  %40 = alloca i32, align 4
  %41 = alloca i32, align 4
  %42 = alloca i32, align 4
  %43 = alloca i32, align 4
  %44 = alloca i32, align 4
  %45 = alloca i32, align 4
  %46 = alloca i32, align 4
  %47 = alloca i32, align 4
  %48 = alloca i32, align 4
  %49 = alloca i32, align 4
  %50 = alloca i32, align 4
  %51 = alloca i32, align 4
  %52 = alloca i32, align 4
  %53 = alloca i32, align 4
  %54 = alloca i32, align 4
  %55 = alloca i32, align 4
  %56 = alloca i32, align 4
  %57 = alloca i32, align 4
  %58 = alloca i16, align 2
  %59 = alloca i16, align 2
  %60 = alloca i32, align 4
  %61 = alloca i32, align 4
  %62 = alloca i32, align 4
  %63 = alloca i32, align 4
  %64 = alloca i32, align 4
  %65 = alloca i32, align 4
  store i32 0, ptr %1, align 4
  store i32 1, ptr %2, align 4, !dbg !357
  store i32 23, ptr %3, align 4, !dbg !358
  store i32 40, ptr %4, align 4, !dbg !359
  store i32 7, ptr %5, align 4, !dbg !360
  store i32 7, ptr %6, align 4, !dbg !361
  %66 = load i32, ptr %5, align 4, !dbg !362
  %67 = load i32, ptr %6, align 4, !dbg !363
  %68 = mul i32 %66, %67, !dbg !364
  store i32 %68, ptr %7, align 4, !dbg !365
  store i32 8, ptr %8, align 4, !dbg !366
  %69 = load i32, ptr %8, align 4, !dbg !367
  %70 = load i32, ptr %7, align 4, !dbg !368
  %71 = mul i32 %69, %70, !dbg !369
  store i32 %71, ptr %9, align 4, !dbg !370
  %72 = load i32, ptr %5, align 4, !dbg !371
  %73 = load i32, ptr %6, align 4, !dbg !372
  %74 = mul i32 %72, %73, !dbg !373
  %75 = load i32, ptr %8, align 4, !dbg !374
  %76 = mul i32 %74, %75, !dbg !375
  store i32 %76, ptr %10, align 4, !dbg !376
  %77 = load i32, ptr %5, align 4, !dbg !377
  %78 = load i32, ptr %6, align 4, !dbg !378
  %79 = mul i32 %77, %78, !dbg !379
  store i32 %79, ptr %11, align 4, !dbg !380
  store i32 2, ptr %12, align 4, !dbg !381
  store i32 300, ptr %13, align 4, !dbg !382
  store i32 4, ptr %14, align 4, !dbg !383
  %80 = load i32, ptr %14, align 4, !dbg !384
  %81 = load i32, ptr %13, align 4, !dbg !385
  %82 = mul i32 %80, %81, !dbg !386
  store i32 %82, ptr %15, align 4, !dbg !387
  %83 = load i32, ptr %3, align 4, !dbg !388
  %84 = load i32, ptr %4, align 4, !dbg !389
  %85 = mul i32 %83, %84, !dbg !390
  %86 = load i32, ptr %9, align 4, !dbg !391
  %87 = mul i32 %85, %86, !dbg !392
  store i32 %87, ptr %16, align 4, !dbg !393
  %88 = load i32, ptr %13, align 4, !dbg !394
  %89 = load i32, ptr %5, align 4, !dbg !395
  %90 = mul i32 %88, %89, !dbg !396
  %91 = load i32, ptr %6, align 4, !dbg !397
  %92 = mul i32 %90, %91, !dbg !398
  %93 = load i32, ptr %8, align 4, !dbg !399
  %94 = mul i32 %92, %93, !dbg !400
  store i32 %94, ptr %17, align 4, !dbg !401
  %95 = load i32, ptr %13, align 4, !dbg !402
  %96 = load i32, ptr %7, align 4, !dbg !403
  %97 = mul i32 %95, %96, !dbg !404
  store i32 %97, ptr %18, align 4, !dbg !405
  %98 = load i32, ptr %16, align 4, !dbg !406
  %99 = call ptr @llvm.stacksave(), !dbg !407
  store ptr %99, ptr %19, align 4, !dbg !407
  %100 = alloca i8, i32 %98, align 1, !dbg !407
  store i32 %98, ptr %20, align 4, !dbg !407
  %101 = load i32, ptr %16, align 4, !dbg !408
  %102 = alloca i8, i32 %101, align 1, !dbg !409
  store i32 %101, ptr %21, align 4, !dbg !409
  %103 = load i32, ptr %15, align 4, !dbg !410
  %104 = alloca i16, i32 %103, align 2, !dbg !411
  store i32 %103, ptr %22, align 4, !dbg !411
  %105 = load i32, ptr %17, align 4, !dbg !412
  %106 = alloca i16, i32 %105, align 2, !dbg !413
  store i32 %105, ptr %23, align 4, !dbg !413
  %107 = load i32, ptr %17, align 4, !dbg !414
  %108 = alloca i16, i32 %107, align 2, !dbg !415
  store i32 %107, ptr %24, align 4, !dbg !415
  %109 = load i32, ptr %17, align 4, !dbg !416
  %110 = alloca i16, i32 %109, align 2, !dbg !417
  store i32 %109, ptr %25, align 4, !dbg !417
  %111 = load i32, ptr %18, align 4, !dbg !418
  %112 = alloca i32, i32 %111, align 4, !dbg !419
  store i32 %111, ptr %26, align 4, !dbg !419
  %113 = load i32, ptr %18, align 4, !dbg !420
  %114 = alloca i32, i32 %113, align 4, !dbg !421
  store i32 %113, ptr %27, align 4, !dbg !421
  %115 = load i32, ptr %18, align 4, !dbg !422
  %116 = alloca i32, i32 %115, align 4, !dbg !423
  store i32 %115, ptr %28, align 4, !dbg !423
  %117 = load i32, ptr %18, align 4, !dbg !424
  %118 = alloca i32, i32 %117, align 4, !dbg !425
  store i32 %117, ptr %29, align 4, !dbg !425
  %119 = load i32, ptr %18, align 4, !dbg !426
  %120 = alloca i32, i32 %119, align 4, !dbg !427
  store i32 %119, ptr %30, align 4, !dbg !427
  %121 = load i32, ptr %18, align 4, !dbg !428
  %122 = alloca i32, i32 %121, align 4, !dbg !429
  store i32 %121, ptr %31, align 4, !dbg !429
  %123 = load i32, ptr %18, align 4, !dbg !430
  %124 = alloca i32, i32 %123, align 4, !dbg !431
  store i32 %123, ptr %32, align 4, !dbg !431
  %125 = load i32, ptr %18, align 4, !dbg !432
  %126 = alloca i32, i32 %125, align 4, !dbg !433
  store i32 %125, ptr %33, align 4, !dbg !433
  %127 = load i32, ptr %18, align 4, !dbg !434
  %128 = alloca i32, i32 %127, align 4, !dbg !435
  store i32 %127, ptr %34, align 4, !dbg !435
  %129 = load i32, ptr %18, align 4, !dbg !436
  %130 = alloca i32, i32 %129, align 4, !dbg !437
  store i32 %129, ptr %35, align 4, !dbg !437
  %131 = load i32, ptr %18, align 4, !dbg !438
  %132 = alloca i32, i32 %131, align 4, !dbg !439
  store i32 %131, ptr %36, align 4, !dbg !439
  %133 = load i32, ptr %18, align 4, !dbg !440
  %134 = alloca i32, i32 %133, align 4, !dbg !441
  store i32 %133, ptr %37, align 4, !dbg !441
  %135 = load i32, ptr %18, align 4, !dbg !442
  %136 = alloca i32, i32 %135, align 4, !dbg !443
  store i32 %135, ptr %38, align 4, !dbg !443
  %137 = load i32, ptr %18, align 4, !dbg !444
  %138 = alloca i32, i32 %137, align 4, !dbg !445
  store i32 %137, ptr %39, align 4, !dbg !445
  %139 = load i32, ptr %18, align 4, !dbg !446
  %140 = alloca i32, i32 %139, align 4, !dbg !447
  store i32 %139, ptr %40, align 4, !dbg !447
  %141 = load i32, ptr %18, align 4, !dbg !448
  %142 = alloca i32, i32 %141, align 4, !dbg !449
  store i32 %141, ptr %41, align 4, !dbg !449
  %143 = load i32, ptr %18, align 4, !dbg !450
  %144 = alloca i8, i32 %143, align 1, !dbg !451
  store i32 %143, ptr %42, align 4, !dbg !451
  %145 = load i32, ptr %18, align 4, !dbg !452
  %146 = alloca i8, i32 %145, align 1, !dbg !453
  store i32 %145, ptr %43, align 4, !dbg !453
  %147 = load i32, ptr %18, align 4, !dbg !454
  %148 = alloca i8, i32 %147, align 1, !dbg !455
  store i32 %147, ptr %44, align 4, !dbg !455
  %149 = load i32, ptr %18, align 4, !dbg !456
  %150 = alloca i8, i32 %149, align 1, !dbg !457
  store i32 %149, ptr %45, align 4, !dbg !457
  %151 = load i32, ptr %18, align 4, !dbg !458
  %152 = alloca i8, i32 %151, align 1, !dbg !459
  store i32 %151, ptr %46, align 4, !dbg !459
  %153 = load i32, ptr %18, align 4, !dbg !460
  %154 = alloca i8, i32 %153, align 1, !dbg !461
  store i32 %153, ptr %47, align 4, !dbg !461
  %155 = load i32, ptr %18, align 4, !dbg !462
  %156 = alloca i8, i32 %155, align 1, !dbg !463
  store i32 %155, ptr %48, align 4, !dbg !463
  %157 = load i32, ptr %18, align 4, !dbg !464
  %158 = alloca i8, i32 %157, align 1, !dbg !465
  store i32 %157, ptr %49, align 4, !dbg !465
  %159 = load i32, ptr %18, align 4, !dbg !466
  %160 = alloca i8, i32 %159, align 1, !dbg !467
  store i32 %159, ptr %50, align 4, !dbg !467
  %161 = load i32, ptr %18, align 4, !dbg !468
  %162 = alloca i8, i32 %161, align 1, !dbg !469
  store i32 %161, ptr %51, align 4, !dbg !469
  %163 = load i32, ptr %18, align 4, !dbg !470
  %164 = alloca i8, i32 %163, align 1, !dbg !471
  store i32 %163, ptr %52, align 4, !dbg !471
  %165 = load i32, ptr %18, align 4, !dbg !472
  %166 = alloca i8, i32 %165, align 1, !dbg !473
  store i32 %165, ptr %53, align 4, !dbg !473
  %167 = load i32, ptr %18, align 4, !dbg !474
  %168 = alloca i8, i32 %167, align 1, !dbg !475
  store i32 %167, ptr %54, align 4, !dbg !475
  %169 = load i32, ptr %18, align 4, !dbg !476
  %170 = alloca i8, i32 %169, align 1, !dbg !477
  store i32 %169, ptr %55, align 4, !dbg !477
  %171 = load i32, ptr %18, align 4, !dbg !478
  %172 = alloca i8, i32 %171, align 1, !dbg !479
  store i32 %171, ptr %56, align 4, !dbg !479
  %173 = load i32, ptr %18, align 4, !dbg !480
  %174 = alloca i8, i32 %173, align 1, !dbg !481
  store i32 %173, ptr %57, align 4, !dbg !481
  %175 = load i32, ptr %3, align 4, !dbg !482
  %176 = sub i32 %175, 1, !dbg !483
  %177 = trunc i32 %176 to i16, !dbg !482
  store i16 %177, ptr %58, align 2, !dbg !484
  %178 = load i32, ptr %4, align 4, !dbg !485
  %179 = sub i32 %178, 1, !dbg !486
  %180 = trunc i32 %179 to i16, !dbg !485
  store i16 %180, ptr %59, align 2, !dbg !487
  %181 = load i32, ptr %13, align 4, !dbg !488
  %182 = alloca i16, i32 %181, align 2, !dbg !489
  store i32 %181, ptr %60, align 4, !dbg !489
  %183 = load i32, ptr %13, align 4, !dbg !490
  %184 = alloca i16, i32 %183, align 2, !dbg !491
  store i32 %183, ptr %61, align 4, !dbg !491
  store i32 0, ptr %62, align 4, !dbg !492
  br label %185, !dbg !493

185:                                              ; preds = %196, %0
  %186 = load i32, ptr %62, align 4, !dbg !494
  %187 = load i32, ptr %13, align 4, !dbg !495
  %188 = icmp ult i32 %186, %187, !dbg !496
  br i1 %188, label %189, label %199, !dbg !497

189:                                              ; preds = %185
  %190 = load i16, ptr %58, align 2, !dbg !498
  %191 = load i32, ptr %62, align 4, !dbg !499
  %192 = getelementptr inbounds i16, ptr %182, i32 %191, !dbg !500
  store i16 %190, ptr %192, align 2, !dbg !501
  %193 = load i16, ptr %59, align 2, !dbg !502
  %194 = load i32, ptr %62, align 4, !dbg !503
  %195 = getelementptr inbounds i16, ptr %184, i32 %194, !dbg !504
  store i16 %193, ptr %195, align 2, !dbg !505
  br label %196, !dbg !506

196:                                              ; preds = %189
  %197 = load i32, ptr %62, align 4, !dbg !507
  %198 = add i32 %197, 1, !dbg !507
  store i32 %198, ptr %62, align 4, !dbg !507
  br label %185, !dbg !497, !llvm.loop !508

199:                                              ; preds = %185
  store i32 0, ptr %63, align 4, !dbg !509
  br label %200, !dbg !510

200:                                              ; preds = %211, %199
  %201 = load i32, ptr %63, align 4, !dbg !511
  %202 = load i32, ptr %16, align 4, !dbg !512
  %203 = icmp ult i32 %201, %202, !dbg !513
  br i1 %203, label %204, label %214, !dbg !514

204:                                              ; preds = %200
  %205 = load i32, ptr %63, align 4, !dbg !515
  %206 = srem i32 %205, 64, !dbg !516
  %207 = shl i32 %206, 2, !dbg !517
  %208 = trunc i32 %207 to i8, !dbg !518
  %209 = load i32, ptr %63, align 4, !dbg !519
  %210 = getelementptr inbounds i8, ptr %100, i32 %209, !dbg !520
  store i8 %208, ptr %210, align 1, !dbg !521
  br label %211, !dbg !522

211:                                              ; preds = %204
  %212 = load i32, ptr %63, align 4, !dbg !523
  %213 = add nsw i32 %212, 1, !dbg !523
  store i32 %213, ptr %63, align 4, !dbg !523
  br label %200, !dbg !514, !llvm.loop !524

214:                                              ; preds = %200
  store i32 0, ptr %64, align 4, !dbg !525
  br label %215, !dbg !526

215:                                              ; preds = %252, %214
  %216 = load i32, ptr %64, align 4, !dbg !527
  %217 = load i32, ptr %13, align 4, !dbg !528
  %218 = udiv i32 %217, 2, !dbg !529
  %219 = icmp ult i32 %216, %218, !dbg !530
  br i1 %219, label %220, label %255, !dbg !531

220:                                              ; preds = %215
  %221 = load i32, ptr %64, align 4, !dbg !532
  %222 = mul nsw i32 %221, 8, !dbg !533
  %223 = getelementptr inbounds i16, ptr %104, i32 %222, !dbg !534
  store i16 16, ptr %223, align 2, !dbg !535
  %224 = load i32, ptr %64, align 4, !dbg !536
  %225 = mul nsw i32 %224, 8, !dbg !537
  %226 = add nsw i32 %225, 1, !dbg !538
  %227 = getelementptr inbounds i16, ptr %104, i32 %226, !dbg !539
  store i16 16, ptr %227, align 2, !dbg !540
  %228 = load i32, ptr %64, align 4, !dbg !541
  %229 = mul nsw i32 %228, 8, !dbg !542
  %230 = add nsw i32 %229, 2, !dbg !543
  %231 = getelementptr inbounds i16, ptr %104, i32 %230, !dbg !544
  store i16 80, ptr %231, align 2, !dbg !545
  %232 = load i32, ptr %64, align 4, !dbg !546
  %233 = mul nsw i32 %232, 8, !dbg !547
  %234 = add nsw i32 %233, 3, !dbg !548
  %235 = getelementptr inbounds i16, ptr %104, i32 %234, !dbg !549
  store i16 80, ptr %235, align 2, !dbg !550
  %236 = load i32, ptr %64, align 4, !dbg !551
  %237 = mul nsw i32 %236, 8, !dbg !552
  %238 = add nsw i32 %237, 4, !dbg !553
  %239 = getelementptr inbounds i16, ptr %104, i32 %238, !dbg !554
  store i16 176, ptr %239, align 2, !dbg !555
  %240 = load i32, ptr %64, align 4, !dbg !556
  %241 = mul nsw i32 %240, 8, !dbg !557
  %242 = add nsw i32 %241, 5, !dbg !558
  %243 = getelementptr inbounds i16, ptr %104, i32 %242, !dbg !559
  store i16 176, ptr %243, align 2, !dbg !560
  %244 = load i32, ptr %64, align 4, !dbg !561
  %245 = mul nsw i32 %244, 8, !dbg !562
  %246 = add nsw i32 %245, 6, !dbg !563
  %247 = getelementptr inbounds i16, ptr %104, i32 %246, !dbg !564
  store i16 240, ptr %247, align 2, !dbg !565
  %248 = load i32, ptr %64, align 4, !dbg !566
  %249 = mul nsw i32 %248, 8, !dbg !567
  %250 = add nsw i32 %249, 7, !dbg !568
  %251 = getelementptr inbounds i16, ptr %104, i32 %250, !dbg !569
  store i16 240, ptr %251, align 2, !dbg !570
  br label %252, !dbg !571

252:                                              ; preds = %220
  %253 = load i32, ptr %64, align 4, !dbg !572
  %254 = add nsw i32 %253, 1, !dbg !572
  store i32 %254, ptr %64, align 4, !dbg !572
  br label %215, !dbg !531, !llvm.loop !573

255:                                              ; preds = %215
  %256 = load i32, ptr %13, align 4, !dbg !574
  %257 = load i32, ptr %7, align 4, !dbg !575
  %258 = load i32, ptr %5, align 4, !dbg !576
  %259 = load i32, ptr %6, align 4, !dbg !577
  %260 = load i32, ptr %8, align 4, !dbg !578
  %261 = load i32, ptr %3, align 4, !dbg !579
  %262 = load i32, ptr %4, align 4, !dbg !580
  %263 = load i32, ptr %10, align 4, !dbg !581
  %264 = load i32, ptr %11, align 4, !dbg !582
  %265 = load i32, ptr %12, align 4, !dbg !583
  %266 = trunc i32 %265 to i8, !dbg !583
  call void @psroi_pooling_align_golden(ptr noundef %100, ptr noundef %104, ptr noundef %106, i32 noundef %256, i32 noundef %257, i32 noundef %258, i32 noundef %259, i32 noundef %260, i32 noundef %261, i32 noundef %262, i32 noundef %263, i32 noundef %264, i8 noundef %266), !dbg !584
  store i32 0, ptr %65, align 4, !dbg !585
  br label %267, !dbg !586

267:                                              ; preds = %292, %255
  %268 = load i32, ptr %65, align 4, !dbg !587
  %269 = load i32, ptr %17, align 4, !dbg !588
  %270 = icmp ult i32 %268, %269, !dbg !589
  br i1 %270, label %271, label %295, !dbg !590

271:                                              ; preds = %267
  %272 = load i32, ptr %65, align 4, !dbg !591
  %273 = getelementptr inbounds i16, ptr %110, i32 %272, !dbg !592
  %274 = load i16, ptr %273, align 2, !dbg !592
  %275 = zext i16 %274 to i32, !dbg !593
  %276 = load i32, ptr %65, align 4, !dbg !594
  %277 = getelementptr inbounds i16, ptr %106, i32 %276, !dbg !595
  %278 = load i16, ptr %277, align 2, !dbg !595
  %279 = zext i16 %278 to i32, !dbg !596
  %280 = icmp eq i32 %275, %279, !dbg !597
  br i1 %280, label %291, label %281, !dbg !598

281:                                              ; preds = %271
  %282 = load i32, ptr %65, align 4, !dbg !599
  %283 = getelementptr inbounds i16, ptr %110, i32 %282, !dbg !600
  %284 = load i16, ptr %283, align 2, !dbg !600
  %285 = zext i16 %284 to i32, !dbg !600
  %286 = load i32, ptr %65, align 4, !dbg !601
  %287 = getelementptr inbounds i16, ptr %106, i32 %286, !dbg !602
  %288 = load i16, ptr %287, align 2, !dbg !602
  %289 = zext i16 %288 to i32, !dbg !602
  %290 = call i32 (ptr, ...) @printf(ptr noundef @.str, i32 noundef %285, i32 noundef %289), !dbg !603
  store i32 0, ptr %2, align 4, !dbg !604
  br label %291, !dbg !605

291:                                              ; preds = %281, %271
  br label %292, !dbg !606

292:                                              ; preds = %291
  %293 = load i32, ptr %65, align 4, !dbg !607
  %294 = add nsw i32 %293, 1, !dbg !607
  store i32 %294, ptr %65, align 4, !dbg !607
  br label %267, !dbg !590, !llvm.loop !608

295:                                              ; preds = %267
  %296 = load i32, ptr %2, align 4, !dbg !609
  %297 = icmp ne i32 %296, 0, !dbg !609
  br i1 %297, label %298, label %300, !dbg !609

298:                                              ; preds = %295
  %299 = call i32 (ptr, ...) @printf(ptr noundef @.str.1), !dbg !610
  br label %300, !dbg !610

300:                                              ; preds = %298, %295
  %301 = load i32, ptr %2, align 4, !dbg !611
  %302 = icmp eq i32 %301, 0, !dbg !612
  %303 = zext i1 %302 to i32, !dbg !612
  store i32 %303, ptr %1, align 4, !dbg !613
  %304 = load ptr, ptr %19, align 4, !dbg !614
  call void @llvm.stackrestore(ptr %304), !dbg !614
  %305 = load i32, ptr %1, align 4, !dbg !614
  ret i32 %305, !dbg !614
}

; Function Attrs: nocallback nofree nosync nounwind willreturn
declare ptr @llvm.stacksave() #1

declare dso_local i32 @printf(ptr noundef, ...) #2

; Function Attrs: nocallback nofree nosync nounwind willreturn
declare void @llvm.stackrestore(ptr) #1

attributes #0 = { noinline nounwind optnone "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+32bit,+a,+c,+m,+relax,-save-restore" }
attributes #1 = { nocallback nofree nosync nounwind willreturn }
attributes #2 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+32bit,+a,+c,+m,+relax,-save-restore" }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!2, !3, !4, !5, !6}
!llvm.ident = !{!7}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 16.0.0 (https://github.com/llvm/llvm-project.git d3b95ecc98d204badbffa1840be7b7a06652a0a3)", isOptimized: false, runtimeVersion: 0, emissionKind: NoDebug, splitDebugInlining: false, nameTableKind: None)
!1 = !DIFile(filename: "psroi_golden.c", directory: "/work/stu/lizhuolun/llvm-project3.0/llvm-project/llvm/lib/Target/PICORIO")
!2 = !{i32 2, !"Debug Info Version", i32 3}
!3 = !{i32 1, !"wchar_size", i32 4}
!4 = !{i32 1, !"target-abi", !"ilp32"}
!5 = !{i32 7, !"frame-pointer", i32 2}
!6 = !{i32 1, !"SmallDataLimit", i32 8}
!7 = !{!"clang version 16.0.0 (https://github.com/llvm/llvm-project.git d3b95ecc98d204badbffa1840be7b7a06652a0a3)"}
!8 = distinct !DISubprogram(name: "bilinear_interpolate", scope: !1, file: !1, line: 29, type: !9, scopeLine: 33, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !10)
!9 = !DISubroutineType(types: !10)
!10 = !{}
!11 = !DILocation(line: 34, column: 14, scope: !8)
!12 = !DILocation(line: 35, column: 10, scope: !8)
!13 = !DILocation(line: 35, column: 12, scope: !8)
!14 = !DILocation(line: 35, column: 29, scope: !8)
!15 = !DILocation(line: 35, column: 26, scope: !8)
!16 = !DILocation(line: 35, column: 36, scope: !8)
!17 = !DILocation(line: 35, column: 40, scope: !8)
!18 = !DILocation(line: 35, column: 42, scope: !8)
!19 = !DILocation(line: 35, column: 59, scope: !8)
!20 = !DILocation(line: 35, column: 56, scope: !8)
!21 = !DILocation(line: 35, column: 9, scope: !8)
!22 = !DILocation(line: 36, column: 9, scope: !8)
!23 = !DILocation(line: 38, column: 9, scope: !8)
!24 = !DILocation(line: 38, column: 11, scope: !8)
!25 = !DILocation(line: 38, column: 17, scope: !8)
!26 = !DILocation(line: 38, column: 16, scope: !8)
!27 = !DILocation(line: 39, column: 9, scope: !8)
!28 = !DILocation(line: 39, column: 11, scope: !8)
!29 = !DILocation(line: 39, column: 17, scope: !8)
!30 = !DILocation(line: 39, column: 16, scope: !8)
!31 = !DILocation(line: 41, column: 18, scope: !8)
!32 = !DILocation(line: 41, column: 20, scope: !8)
!33 = !DILocation(line: 41, column: 9, scope: !8)
!34 = !DILocation(line: 42, column: 18, scope: !8)
!35 = !DILocation(line: 42, column: 20, scope: !8)
!36 = !DILocation(line: 42, column: 9, scope: !8)
!37 = !DILocation(line: 46, column: 9, scope: !8)
!38 = !DILocation(line: 46, column: 18, scope: !8)
!39 = !DILocation(line: 46, column: 25, scope: !8)
!40 = !DILocation(line: 46, column: 15, scope: !8)
!41 = !DILocation(line: 47, column: 26, scope: !8)
!42 = !DILocation(line: 47, column: 33, scope: !8)
!43 = !DILocation(line: 47, column: 24, scope: !8)
!44 = !DILocation(line: 47, column: 16, scope: !8)
!45 = !DILocation(line: 48, column: 13, scope: !8)
!46 = !DILocation(line: 48, column: 19, scope: !8)
!47 = !DILocation(line: 48, column: 11, scope: !8)
!48 = !DILocation(line: 49, column: 5, scope: !8)
!49 = !DILocation(line: 50, column: 18, scope: !8)
!50 = !DILocation(line: 50, column: 24, scope: !8)
!51 = !DILocation(line: 50, column: 16, scope: !8)
!52 = !DILocation(line: 53, column: 9, scope: !8)
!53 = !DILocation(line: 53, column: 18, scope: !8)
!54 = !DILocation(line: 53, column: 24, scope: !8)
!55 = !DILocation(line: 53, column: 15, scope: !8)
!56 = !DILocation(line: 54, column: 26, scope: !8)
!57 = !DILocation(line: 54, column: 32, scope: !8)
!58 = !DILocation(line: 54, column: 24, scope: !8)
!59 = !DILocation(line: 54, column: 16, scope: !8)
!60 = !DILocation(line: 55, column: 13, scope: !8)
!61 = !DILocation(line: 55, column: 19, scope: !8)
!62 = !DILocation(line: 55, column: 11, scope: !8)
!63 = !DILocation(line: 56, column: 5, scope: !8)
!64 = !DILocation(line: 57, column: 18, scope: !8)
!65 = !DILocation(line: 57, column: 24, scope: !8)
!66 = !DILocation(line: 57, column: 16, scope: !8)
!67 = !DILocation(line: 60, column: 19, scope: !8)
!68 = !DILocation(line: 60, column: 24, scope: !8)
!69 = !DILocation(line: 60, column: 30, scope: !8)
!70 = !DILocation(line: 60, column: 21, scope: !8)
!71 = !DILocation(line: 60, column: 14, scope: !8)
!72 = !DILocation(line: 61, column: 19, scope: !8)
!73 = !DILocation(line: 61, column: 24, scope: !8)
!74 = !DILocation(line: 61, column: 30, scope: !8)
!75 = !DILocation(line: 61, column: 21, scope: !8)
!76 = !DILocation(line: 61, column: 14, scope: !8)
!77 = !DILocation(line: 62, column: 38, scope: !8)
!78 = !DILocation(line: 62, column: 36, scope: !8)
!79 = !DILocation(line: 62, column: 19, scope: !8)
!80 = !DILocation(line: 62, column: 14, scope: !8)
!81 = !DILocation(line: 63, column: 38, scope: !8)
!82 = !DILocation(line: 63, column: 36, scope: !8)
!83 = !DILocation(line: 63, column: 19, scope: !8)
!84 = !DILocation(line: 63, column: 14, scope: !8)
!85 = !DILocation(line: 64, column: 19, scope: !8)
!86 = !DILocation(line: 64, column: 24, scope: !8)
!87 = !DILocation(line: 64, column: 30, scope: !8)
!88 = !DILocation(line: 64, column: 29, scope: !8)
!89 = !DILocation(line: 64, column: 38, scope: !8)
!90 = !DILocation(line: 64, column: 36, scope: !8)
!91 = !DILocation(line: 64, column: 14, scope: !8)
!92 = !DILocation(line: 65, column: 19, scope: !8)
!93 = !DILocation(line: 65, column: 24, scope: !8)
!94 = !DILocation(line: 65, column: 30, scope: !8)
!95 = !DILocation(line: 65, column: 29, scope: !8)
!96 = !DILocation(line: 65, column: 38, scope: !8)
!97 = !DILocation(line: 65, column: 36, scope: !8)
!98 = !DILocation(line: 65, column: 14, scope: !8)
!99 = !DILocation(line: 66, column: 19, scope: !8)
!100 = !DILocation(line: 66, column: 24, scope: !8)
!101 = !DILocation(line: 66, column: 31, scope: !8)
!102 = !DILocation(line: 66, column: 30, scope: !8)
!103 = !DILocation(line: 66, column: 39, scope: !8)
!104 = !DILocation(line: 66, column: 37, scope: !8)
!105 = !DILocation(line: 66, column: 14, scope: !8)
!106 = !DILocation(line: 67, column: 19, scope: !8)
!107 = !DILocation(line: 67, column: 24, scope: !8)
!108 = !DILocation(line: 67, column: 31, scope: !8)
!109 = !DILocation(line: 67, column: 30, scope: !8)
!110 = !DILocation(line: 67, column: 39, scope: !8)
!111 = !DILocation(line: 67, column: 37, scope: !8)
!112 = !DILocation(line: 67, column: 14, scope: !8)
!113 = !DILocation(line: 69, column: 20, scope: !8)
!114 = !DILocation(line: 69, column: 23, scope: !8)
!115 = !DILocation(line: 69, column: 22, scope: !8)
!116 = !DILocation(line: 69, column: 27, scope: !8)
!117 = !DILocation(line: 69, column: 19, scope: !8)
!118 = !DILocation(line: 69, column: 14, scope: !8)
!119 = !DILocation(line: 70, column: 20, scope: !8)
!120 = !DILocation(line: 70, column: 23, scope: !8)
!121 = !DILocation(line: 70, column: 22, scope: !8)
!122 = !DILocation(line: 70, column: 27, scope: !8)
!123 = !DILocation(line: 70, column: 19, scope: !8)
!124 = !DILocation(line: 70, column: 14, scope: !8)
!125 = !DILocation(line: 71, column: 20, scope: !8)
!126 = !DILocation(line: 71, column: 23, scope: !8)
!127 = !DILocation(line: 71, column: 22, scope: !8)
!128 = !DILocation(line: 71, column: 27, scope: !8)
!129 = !DILocation(line: 71, column: 19, scope: !8)
!130 = !DILocation(line: 71, column: 14, scope: !8)
!131 = !DILocation(line: 72, column: 20, scope: !8)
!132 = !DILocation(line: 72, column: 23, scope: !8)
!133 = !DILocation(line: 72, column: 22, scope: !8)
!134 = !DILocation(line: 72, column: 27, scope: !8)
!135 = !DILocation(line: 72, column: 19, scope: !8)
!136 = !DILocation(line: 72, column: 14, scope: !8)
!137 = !DILocation(line: 75, column: 23, scope: !8)
!138 = !DILocation(line: 75, column: 26, scope: !8)
!139 = !DILocation(line: 75, column: 25, scope: !8)
!140 = !DILocation(line: 75, column: 31, scope: !8)
!141 = !DILocation(line: 75, column: 34, scope: !8)
!142 = !DILocation(line: 75, column: 33, scope: !8)
!143 = !DILocation(line: 75, column: 29, scope: !8)
!144 = !DILocation(line: 75, column: 39, scope: !8)
!145 = !DILocation(line: 75, column: 42, scope: !8)
!146 = !DILocation(line: 75, column: 41, scope: !8)
!147 = !DILocation(line: 75, column: 37, scope: !8)
!148 = !DILocation(line: 75, column: 47, scope: !8)
!149 = !DILocation(line: 75, column: 50, scope: !8)
!150 = !DILocation(line: 75, column: 49, scope: !8)
!151 = !DILocation(line: 75, column: 45, scope: !8)
!152 = !DILocation(line: 75, column: 55, scope: !8)
!153 = !DILocation(line: 75, column: 9, scope: !8)
!154 = !DILocation(line: 77, column: 12, scope: !8)
!155 = !DILocation(line: 77, column: 5, scope: !8)
!156 = !DILocation(line: 78, column: 1, scope: !8)
!157 = distinct !DISubprogram(name: "psroi_align_golden", scope: !1, file: !1, line: 80, type: !9, scopeLine: 92, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !10)
!158 = !DILocation(line: 93, column: 25, scope: !157)
!159 = !DILocation(line: 93, column: 14, scope: !157)
!160 = !DILocation(line: 95, column: 19, scope: !157)
!161 = !DILocation(line: 95, column: 30, scope: !157)
!162 = !DILocation(line: 95, column: 28, scope: !157)
!163 = !DILocation(line: 95, column: 14, scope: !157)
!164 = !DILocation(line: 96, column: 19, scope: !157)
!165 = !DILocation(line: 96, column: 30, scope: !157)
!166 = !DILocation(line: 96, column: 28, scope: !157)
!167 = !DILocation(line: 96, column: 14, scope: !157)
!168 = !DILocation(line: 98, column: 19, scope: !157)
!169 = !DILocation(line: 98, column: 10, scope: !157)
!170 = !DILocation(line: 98, column: 24, scope: !157)
!171 = !DILocation(line: 98, column: 26, scope: !157)
!172 = !DILocation(line: 98, column: 25, scope: !157)
!173 = !DILocation(line: 98, column: 5, scope: !157)
!174 = !DILocation(line: 99, column: 20, scope: !157)
!175 = !DILocation(line: 99, column: 18, scope: !157)
!176 = !DILocation(line: 100, column: 32, scope: !157)
!177 = !DILocation(line: 100, column: 45, scope: !157)
!178 = !DILocation(line: 100, column: 43, scope: !157)
!179 = !DILocation(line: 100, column: 18, scope: !157)
!180 = !DILocation(line: 101, column: 32, scope: !157)
!181 = !DILocation(line: 101, column: 45, scope: !157)
!182 = !DILocation(line: 101, column: 43, scope: !157)
!183 = !DILocation(line: 101, column: 47, scope: !157)
!184 = !DILocation(line: 101, column: 18, scope: !157)
!185 = !DILocation(line: 102, column: 30, scope: !157)
!186 = !DILocation(line: 102, column: 43, scope: !157)
!187 = !DILocation(line: 102, column: 41, scope: !157)
!188 = !DILocation(line: 102, column: 45, scope: !157)
!189 = !DILocation(line: 102, column: 18, scope: !157)
!190 = !DILocation(line: 103, column: 30, scope: !157)
!191 = !DILocation(line: 103, column: 43, scope: !157)
!192 = !DILocation(line: 103, column: 41, scope: !157)
!193 = !DILocation(line: 103, column: 45, scope: !157)
!194 = !DILocation(line: 103, column: 18, scope: !157)
!195 = !DILocation(line: 105, column: 30, scope: !157)
!196 = !DILocation(line: 105, column: 18, scope: !157)
!197 = !DILocation(line: 106, column: 31, scope: !157)
!198 = !DILocation(line: 106, column: 18, scope: !157)
!199 = !DILocation(line: 108, column: 55, scope: !157)
!200 = !DILocation(line: 108, column: 53, scope: !157)
!201 = !DILocation(line: 108, column: 65, scope: !157)
!202 = !DILocation(line: 108, column: 37, scope: !157)
!203 = !DILocation(line: 108, column: 18, scope: !157)
!204 = !DILocation(line: 109, column: 55, scope: !157)
!205 = !DILocation(line: 109, column: 53, scope: !157)
!206 = !DILocation(line: 109, column: 65, scope: !157)
!207 = !DILocation(line: 109, column: 37, scope: !157)
!208 = !DILocation(line: 109, column: 18, scope: !157)
!209 = !DILocation(line: 110, column: 32, scope: !157)
!210 = !DILocation(line: 110, column: 44, scope: !157)
!211 = !DILocation(line: 110, column: 42, scope: !157)
!212 = !DILocation(line: 110, column: 62, scope: !157)
!213 = !DILocation(line: 110, column: 31, scope: !157)
!214 = !DILocation(line: 110, column: 18, scope: !157)
!215 = !DILocation(line: 111, column: 32, scope: !157)
!216 = !DILocation(line: 111, column: 45, scope: !157)
!217 = !DILocation(line: 111, column: 43, scope: !157)
!218 = !DILocation(line: 111, column: 63, scope: !157)
!219 = !DILocation(line: 111, column: 31, scope: !157)
!220 = !DILocation(line: 111, column: 18, scope: !157)
!221 = !DILocation(line: 113, column: 30, scope: !157)
!222 = !DILocation(line: 113, column: 13, scope: !157)
!223 = !DILocation(line: 114, column: 30, scope: !157)
!224 = !DILocation(line: 114, column: 13, scope: !157)
!225 = !DILocation(line: 116, column: 27, scope: !157)
!226 = !DILocation(line: 116, column: 41, scope: !157)
!227 = !DILocation(line: 116, column: 56, scope: !157)
!228 = !DILocation(line: 116, column: 38, scope: !157)
!229 = !DILocation(line: 116, column: 18, scope: !157)
!230 = !DILocation(line: 117, column: 27, scope: !157)
!231 = !DILocation(line: 117, column: 41, scope: !157)
!232 = !DILocation(line: 117, column: 56, scope: !157)
!233 = !DILocation(line: 117, column: 38, scope: !157)
!234 = !DILocation(line: 117, column: 18, scope: !157)
!235 = !DILocation(line: 120, column: 18, scope: !157)
!236 = !DILocation(line: 120, column: 14, scope: !157)
!237 = !DILocation(line: 120, column: 25, scope: !157)
!238 = !DILocation(line: 120, column: 29, scope: !157)
!239 = !DILocation(line: 120, column: 27, scope: !157)
!240 = !DILocation(line: 120, column: 9, scope: !157)
!241 = !DILocation(line: 121, column: 22, scope: !157)
!242 = !DILocation(line: 122, column: 30, scope: !157)
!243 = !DILocation(line: 122, column: 32, scope: !157)
!244 = !DILocation(line: 122, column: 31, scope: !157)
!245 = !DILocation(line: 122, column: 50, scope: !157)
!246 = !DILocation(line: 122, column: 52, scope: !157)
!247 = !DILocation(line: 122, column: 51, scope: !157)
!248 = !DILocation(line: 122, column: 48, scope: !157)
!249 = !DILocation(line: 122, column: 64, scope: !157)
!250 = !DILocation(line: 122, column: 67, scope: !157)
!251 = !DILocation(line: 122, column: 66, scope: !157)
!252 = !DILocation(line: 122, column: 62, scope: !157)
!253 = !DILocation(line: 122, column: 78, scope: !157)
!254 = !DILocation(line: 122, column: 76, scope: !157)
!255 = !DILocation(line: 122, column: 22, scope: !157)
!256 = !DILocation(line: 123, column: 22, scope: !157)
!257 = !DILocation(line: 123, column: 18, scope: !157)
!258 = !DILocation(line: 123, column: 30, scope: !157)
!259 = !DILocation(line: 123, column: 35, scope: !157)
!260 = !DILocation(line: 123, column: 33, scope: !157)
!261 = !DILocation(line: 123, column: 13, scope: !157)
!262 = !DILocation(line: 124, column: 30, scope: !157)
!263 = !DILocation(line: 124, column: 44, scope: !157)
!264 = !DILocation(line: 124, column: 49, scope: !157)
!265 = !DILocation(line: 124, column: 47, scope: !157)
!266 = !DILocation(line: 124, column: 42, scope: !157)
!267 = !DILocation(line: 124, column: 65, scope: !157)
!268 = !DILocation(line: 124, column: 64, scope: !157)
!269 = !DILocation(line: 124, column: 68, scope: !157)
!270 = !DILocation(line: 124, column: 73, scope: !157)
!271 = !DILocation(line: 124, column: 72, scope: !157)
!272 = !DILocation(line: 124, column: 60, scope: !157)
!273 = !DILocation(line: 124, column: 26, scope: !157)
!274 = !DILocation(line: 125, column: 26, scope: !157)
!275 = !DILocation(line: 125, column: 22, scope: !157)
!276 = !DILocation(line: 125, column: 34, scope: !157)
!277 = !DILocation(line: 125, column: 39, scope: !157)
!278 = !DILocation(line: 125, column: 37, scope: !157)
!279 = !DILocation(line: 125, column: 17, scope: !157)
!280 = !DILocation(line: 126, column: 34, scope: !157)
!281 = !DILocation(line: 126, column: 48, scope: !157)
!282 = !DILocation(line: 126, column: 53, scope: !157)
!283 = !DILocation(line: 126, column: 51, scope: !157)
!284 = !DILocation(line: 126, column: 46, scope: !157)
!285 = !DILocation(line: 126, column: 69, scope: !157)
!286 = !DILocation(line: 126, column: 68, scope: !157)
!287 = !DILocation(line: 126, column: 72, scope: !157)
!288 = !DILocation(line: 126, column: 77, scope: !157)
!289 = !DILocation(line: 126, column: 76, scope: !157)
!290 = !DILocation(line: 126, column: 64, scope: !157)
!291 = !DILocation(line: 126, column: 30, scope: !157)
!292 = !DILocation(line: 128, column: 25, scope: !157)
!293 = !DILocation(line: 129, column: 25, scope: !157)
!294 = !DILocation(line: 130, column: 25, scope: !157)
!295 = !DILocation(line: 131, column: 25, scope: !157)
!296 = !DILocation(line: 132, column: 25, scope: !157)
!297 = !DILocation(line: 127, column: 35, scope: !157)
!298 = !DILocation(line: 127, column: 32, scope: !157)
!299 = !DILocation(line: 133, column: 17, scope: !157)
!300 = !DILocation(line: 125, column: 57, scope: !157)
!301 = distinct !{!301, !279, !299, !302}
!302 = !{!"llvm.loop.mustprogress"}
!303 = !DILocation(line: 134, column: 13, scope: !157)
!304 = !DILocation(line: 123, column: 53, scope: !157)
!305 = distinct !{!305, !261, !303, !302}
!306 = !DILocation(line: 136, column: 42, scope: !157)
!307 = !DILocation(line: 136, column: 53, scope: !157)
!308 = !DILocation(line: 136, column: 31, scope: !157)
!309 = !DILocation(line: 136, column: 13, scope: !157)
!310 = !DILocation(line: 136, column: 22, scope: !157)
!311 = !DILocation(line: 136, column: 29, scope: !157)
!312 = !DILocation(line: 137, column: 25, scope: !157)
!313 = !DILocation(line: 137, column: 38, scope: !157)
!314 = !DILocation(line: 137, column: 36, scope: !157)
!315 = !DILocation(line: 137, column: 22, scope: !157)
!316 = !DILocation(line: 138, column: 9, scope: !157)
!317 = !DILocation(line: 120, column: 40, scope: !157)
!318 = distinct !{!318, !240, !316, !302}
!319 = !DILocation(line: 139, column: 5, scope: !157)
!320 = !DILocation(line: 98, column: 37, scope: !157)
!321 = distinct !{!321, !173, !319, !302}
!322 = !DILocation(line: 140, column: 1, scope: !157)
!323 = distinct !DISubprogram(name: "psroi_pooling_align_golden", scope: !1, file: !1, line: 142, type: !9, scopeLine: 154, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !10)
!324 = !DILocation(line: 155, column: 24, scope: !323)
!325 = !DILocation(line: 155, column: 14, scope: !323)
!326 = !DILocation(line: 156, column: 19, scope: !323)
!327 = !DILocation(line: 156, column: 10, scope: !323)
!328 = !DILocation(line: 156, column: 24, scope: !323)
!329 = !DILocation(line: 156, column: 26, scope: !323)
!330 = !DILocation(line: 156, column: 25, scope: !323)
!331 = !DILocation(line: 156, column: 5, scope: !323)
!332 = !DILocation(line: 157, column: 28, scope: !323)
!333 = !DILocation(line: 158, column: 29, scope: !323)
!334 = !DILocation(line: 159, column: 29, scope: !323)
!335 = !DILocation(line: 160, column: 29, scope: !323)
!336 = !DILocation(line: 161, column: 29, scope: !323)
!337 = !DILocation(line: 162, column: 29, scope: !323)
!338 = !DILocation(line: 163, column: 29, scope: !323)
!339 = !DILocation(line: 164, column: 29, scope: !323)
!340 = !DILocation(line: 165, column: 29, scope: !323)
!341 = !DILocation(line: 166, column: 29, scope: !323)
!342 = !DILocation(line: 167, column: 29, scope: !323)
!343 = !DILocation(line: 168, column: 29, scope: !323)
!344 = !DILocation(line: 169, column: 29, scope: !323)
!345 = !DILocation(line: 157, column: 9, scope: !323)
!346 = !DILocation(line: 170, column: 20, scope: !323)
!347 = !DILocation(line: 170, column: 33, scope: !323)
!348 = !DILocation(line: 170, column: 31, scope: !323)
!349 = !DILocation(line: 170, column: 45, scope: !323)
!350 = !DILocation(line: 170, column: 43, scope: !323)
!351 = !DILocation(line: 170, column: 17, scope: !323)
!352 = !DILocation(line: 171, column: 5, scope: !323)
!353 = !DILocation(line: 156, column: 35, scope: !323)
!354 = distinct !{!354, !331, !352, !302}
!355 = !DILocation(line: 172, column: 1, scope: !323)
!356 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 174, type: !9, scopeLine: 174, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !10)
!357 = !DILocation(line: 176, column: 9, scope: !356)
!358 = !DILocation(line: 177, column: 14, scope: !356)
!359 = !DILocation(line: 178, column: 14, scope: !356)
!360 = !DILocation(line: 179, column: 14, scope: !356)
!361 = !DILocation(line: 180, column: 14, scope: !356)
!362 = !DILocation(line: 181, column: 23, scope: !356)
!363 = !DILocation(line: 181, column: 34, scope: !356)
!364 = !DILocation(line: 181, column: 32, scope: !356)
!365 = !DILocation(line: 181, column: 14, scope: !356)
!366 = !DILocation(line: 182, column: 14, scope: !356)
!367 = !DILocation(line: 183, column: 28, scope: !356)
!368 = !DILocation(line: 183, column: 39, scope: !356)
!369 = !DILocation(line: 183, column: 37, scope: !356)
!370 = !DILocation(line: 183, column: 14, scope: !356)
!371 = !DILocation(line: 184, column: 32, scope: !356)
!372 = !DILocation(line: 184, column: 43, scope: !356)
!373 = !DILocation(line: 184, column: 41, scope: !356)
!374 = !DILocation(line: 184, column: 54, scope: !356)
!375 = !DILocation(line: 184, column: 52, scope: !356)
!376 = !DILocation(line: 184, column: 14, scope: !356)
!377 = !DILocation(line: 185, column: 30, scope: !356)
!378 = !DILocation(line: 185, column: 41, scope: !356)
!379 = !DILocation(line: 185, column: 39, scope: !356)
!380 = !DILocation(line: 185, column: 14, scope: !356)
!381 = !DILocation(line: 186, column: 14, scope: !356)
!382 = !DILocation(line: 188, column: 14, scope: !356)
!383 = !DILocation(line: 189, column: 14, scope: !356)
!384 = !DILocation(line: 191, column: 25, scope: !356)
!385 = !DILocation(line: 191, column: 40, scope: !356)
!386 = !DILocation(line: 191, column: 38, scope: !356)
!387 = !DILocation(line: 191, column: 14, scope: !356)
!388 = !DILocation(line: 192, column: 25, scope: !356)
!389 = !DILocation(line: 192, column: 38, scope: !356)
!390 = !DILocation(line: 192, column: 36, scope: !356)
!391 = !DILocation(line: 192, column: 50, scope: !356)
!392 = !DILocation(line: 192, column: 48, scope: !356)
!393 = !DILocation(line: 192, column: 14, scope: !356)
!394 = !DILocation(line: 193, column: 25, scope: !356)
!395 = !DILocation(line: 193, column: 34, scope: !356)
!396 = !DILocation(line: 193, column: 33, scope: !356)
!397 = !DILocation(line: 193, column: 45, scope: !356)
!398 = !DILocation(line: 193, column: 43, scope: !356)
!399 = !DILocation(line: 193, column: 56, scope: !356)
!400 = !DILocation(line: 193, column: 54, scope: !356)
!401 = !DILocation(line: 193, column: 14, scope: !356)
!402 = !DILocation(line: 194, column: 27, scope: !356)
!403 = !DILocation(line: 194, column: 38, scope: !356)
!404 = !DILocation(line: 194, column: 36, scope: !356)
!405 = !DILocation(line: 194, column: 14, scope: !356)
!406 = !DILocation(line: 196, column: 21, scope: !356)
!407 = !DILocation(line: 196, column: 5, scope: !356)
!408 = !DILocation(line: 197, column: 26, scope: !356)
!409 = !DILocation(line: 197, column: 5, scope: !356)
!410 = !DILocation(line: 198, column: 22, scope: !356)
!411 = !DILocation(line: 198, column: 5, scope: !356)
!412 = !DILocation(line: 199, column: 22, scope: !356)
!413 = !DILocation(line: 199, column: 5, scope: !356)
!414 = !DILocation(line: 200, column: 26, scope: !356)
!415 = !DILocation(line: 200, column: 5, scope: !356)
!416 = !DILocation(line: 201, column: 31, scope: !356)
!417 = !DILocation(line: 201, column: 5, scope: !356)
!418 = !DILocation(line: 203, column: 27, scope: !356)
!419 = !DILocation(line: 203, column: 5, scope: !356)
!420 = !DILocation(line: 204, column: 24, scope: !356)
!421 = !DILocation(line: 204, column: 2, scope: !356)
!422 = !DILocation(line: 205, column: 24, scope: !356)
!423 = !DILocation(line: 205, column: 2, scope: !356)
!424 = !DILocation(line: 206, column: 24, scope: !356)
!425 = !DILocation(line: 206, column: 2, scope: !356)
!426 = !DILocation(line: 207, column: 24, scope: !356)
!427 = !DILocation(line: 207, column: 2, scope: !356)
!428 = !DILocation(line: 208, column: 24, scope: !356)
!429 = !DILocation(line: 208, column: 2, scope: !356)
!430 = !DILocation(line: 209, column: 24, scope: !356)
!431 = !DILocation(line: 209, column: 2, scope: !356)
!432 = !DILocation(line: 210, column: 24, scope: !356)
!433 = !DILocation(line: 210, column: 2, scope: !356)
!434 = !DILocation(line: 211, column: 24, scope: !356)
!435 = !DILocation(line: 211, column: 2, scope: !356)
!436 = !DILocation(line: 212, column: 24, scope: !356)
!437 = !DILocation(line: 212, column: 2, scope: !356)
!438 = !DILocation(line: 213, column: 25, scope: !356)
!439 = !DILocation(line: 213, column: 2, scope: !356)
!440 = !DILocation(line: 214, column: 25, scope: !356)
!441 = !DILocation(line: 214, column: 2, scope: !356)
!442 = !DILocation(line: 215, column: 25, scope: !356)
!443 = !DILocation(line: 215, column: 2, scope: !356)
!444 = !DILocation(line: 216, column: 25, scope: !356)
!445 = !DILocation(line: 216, column: 2, scope: !356)
!446 = !DILocation(line: 217, column: 25, scope: !356)
!447 = !DILocation(line: 217, column: 2, scope: !356)
!448 = !DILocation(line: 218, column: 25, scope: !356)
!449 = !DILocation(line: 218, column: 2, scope: !356)
!450 = !DILocation(line: 220, column: 23, scope: !356)
!451 = !DILocation(line: 220, column: 2, scope: !356)
!452 = !DILocation(line: 221, column: 23, scope: !356)
!453 = !DILocation(line: 221, column: 2, scope: !356)
!454 = !DILocation(line: 222, column: 23, scope: !356)
!455 = !DILocation(line: 222, column: 2, scope: !356)
!456 = !DILocation(line: 223, column: 23, scope: !356)
!457 = !DILocation(line: 223, column: 2, scope: !356)
!458 = !DILocation(line: 224, column: 23, scope: !356)
!459 = !DILocation(line: 224, column: 2, scope: !356)
!460 = !DILocation(line: 225, column: 23, scope: !356)
!461 = !DILocation(line: 225, column: 2, scope: !356)
!462 = !DILocation(line: 226, column: 23, scope: !356)
!463 = !DILocation(line: 226, column: 2, scope: !356)
!464 = !DILocation(line: 227, column: 23, scope: !356)
!465 = !DILocation(line: 227, column: 2, scope: !356)
!466 = !DILocation(line: 228, column: 23, scope: !356)
!467 = !DILocation(line: 228, column: 2, scope: !356)
!468 = !DILocation(line: 229, column: 23, scope: !356)
!469 = !DILocation(line: 229, column: 2, scope: !356)
!470 = !DILocation(line: 230, column: 24, scope: !356)
!471 = !DILocation(line: 230, column: 2, scope: !356)
!472 = !DILocation(line: 231, column: 24, scope: !356)
!473 = !DILocation(line: 231, column: 2, scope: !356)
!474 = !DILocation(line: 232, column: 24, scope: !356)
!475 = !DILocation(line: 232, column: 2, scope: !356)
!476 = !DILocation(line: 233, column: 24, scope: !356)
!477 = !DILocation(line: 233, column: 2, scope: !356)
!478 = !DILocation(line: 234, column: 24, scope: !356)
!479 = !DILocation(line: 234, column: 2, scope: !356)
!480 = !DILocation(line: 235, column: 24, scope: !356)
!481 = !DILocation(line: 235, column: 2, scope: !356)
!482 = !DILocation(line: 237, column: 30, scope: !356)
!483 = !DILocation(line: 237, column: 41, scope: !356)
!484 = !DILocation(line: 237, column: 14, scope: !356)
!485 = !DILocation(line: 238, column: 29, scope: !356)
!486 = !DILocation(line: 238, column: 39, scope: !356)
!487 = !DILocation(line: 238, column: 14, scope: !356)
!488 = !DILocation(line: 241, column: 35, scope: !356)
!489 = !DILocation(line: 241, column: 5, scope: !356)
!490 = !DILocation(line: 242, column: 34, scope: !356)
!491 = !DILocation(line: 242, column: 5, scope: !356)
!492 = !DILocation(line: 243, column: 19, scope: !356)
!493 = !DILocation(line: 243, column: 10, scope: !356)
!494 = !DILocation(line: 243, column: 26, scope: !356)
!495 = !DILocation(line: 243, column: 30, scope: !356)
!496 = !DILocation(line: 243, column: 28, scope: !356)
!497 = !DILocation(line: 243, column: 5, scope: !356)
!498 = !DILocation(line: 244, column: 35, scope: !356)
!499 = !DILocation(line: 244, column: 30, scope: !356)
!500 = !DILocation(line: 244, column: 9, scope: !356)
!501 = !DILocation(line: 244, column: 33, scope: !356)
!502 = !DILocation(line: 245, column: 34, scope: !356)
!503 = !DILocation(line: 245, column: 29, scope: !356)
!504 = !DILocation(line: 245, column: 9, scope: !356)
!505 = !DILocation(line: 245, column: 32, scope: !356)
!506 = !DILocation(line: 246, column: 5, scope: !356)
!507 = !DILocation(line: 243, column: 41, scope: !356)
!508 = distinct !{!508, !497, !506, !302}
!509 = !DILocation(line: 248, column: 14, scope: !356)
!510 = !DILocation(line: 248, column: 10, scope: !356)
!511 = !DILocation(line: 248, column: 19, scope: !356)
!512 = !DILocation(line: 248, column: 21, scope: !356)
!513 = !DILocation(line: 248, column: 20, scope: !356)
!514 = !DILocation(line: 248, column: 5, scope: !356)
!515 = !DILocation(line: 250, column: 24, scope: !356)
!516 = !DILocation(line: 250, column: 26, scope: !356)
!517 = !DILocation(line: 250, column: 32, scope: !356)
!518 = !DILocation(line: 250, column: 22, scope: !356)
!519 = !DILocation(line: 250, column: 17, scope: !356)
!520 = !DILocation(line: 250, column: 9, scope: !356)
!521 = !DILocation(line: 250, column: 20, scope: !356)
!522 = !DILocation(line: 254, column: 5, scope: !356)
!523 = !DILocation(line: 248, column: 32, scope: !356)
!524 = distinct !{!524, !514, !522, !302}
!525 = !DILocation(line: 256, column: 13, scope: !356)
!526 = !DILocation(line: 256, column: 9, scope: !356)
!527 = !DILocation(line: 256, column: 18, scope: !356)
!528 = !DILocation(line: 256, column: 21, scope: !356)
!529 = !DILocation(line: 256, column: 29, scope: !356)
!530 = !DILocation(line: 256, column: 19, scope: !356)
!531 = !DILocation(line: 256, column: 5, scope: !356)
!532 = !DILocation(line: 257, column: 17, scope: !356)
!533 = !DILocation(line: 257, column: 18, scope: !356)
!534 = !DILocation(line: 257, column: 9, scope: !356)
!535 = !DILocation(line: 257, column: 24, scope: !356)
!536 = !DILocation(line: 258, column: 17, scope: !356)
!537 = !DILocation(line: 258, column: 18, scope: !356)
!538 = !DILocation(line: 258, column: 20, scope: !356)
!539 = !DILocation(line: 258, column: 9, scope: !356)
!540 = !DILocation(line: 258, column: 24, scope: !356)
!541 = !DILocation(line: 259, column: 17, scope: !356)
!542 = !DILocation(line: 259, column: 18, scope: !356)
!543 = !DILocation(line: 259, column: 20, scope: !356)
!544 = !DILocation(line: 259, column: 9, scope: !356)
!545 = !DILocation(line: 259, column: 24, scope: !356)
!546 = !DILocation(line: 260, column: 17, scope: !356)
!547 = !DILocation(line: 260, column: 18, scope: !356)
!548 = !DILocation(line: 260, column: 20, scope: !356)
!549 = !DILocation(line: 260, column: 9, scope: !356)
!550 = !DILocation(line: 260, column: 24, scope: !356)
!551 = !DILocation(line: 262, column: 17, scope: !356)
!552 = !DILocation(line: 262, column: 18, scope: !356)
!553 = !DILocation(line: 262, column: 20, scope: !356)
!554 = !DILocation(line: 262, column: 9, scope: !356)
!555 = !DILocation(line: 262, column: 24, scope: !356)
!556 = !DILocation(line: 263, column: 17, scope: !356)
!557 = !DILocation(line: 263, column: 18, scope: !356)
!558 = !DILocation(line: 263, column: 20, scope: !356)
!559 = !DILocation(line: 263, column: 9, scope: !356)
!560 = !DILocation(line: 263, column: 24, scope: !356)
!561 = !DILocation(line: 264, column: 17, scope: !356)
!562 = !DILocation(line: 264, column: 18, scope: !356)
!563 = !DILocation(line: 264, column: 20, scope: !356)
!564 = !DILocation(line: 264, column: 9, scope: !356)
!565 = !DILocation(line: 264, column: 24, scope: !356)
!566 = !DILocation(line: 265, column: 17, scope: !356)
!567 = !DILocation(line: 265, column: 18, scope: !356)
!568 = !DILocation(line: 265, column: 20, scope: !356)
!569 = !DILocation(line: 265, column: 9, scope: !356)
!570 = !DILocation(line: 265, column: 24, scope: !356)
!571 = !DILocation(line: 266, column: 5, scope: !356)
!572 = !DILocation(line: 256, column: 34, scope: !356)
!573 = distinct !{!573, !531, !571, !302}
!574 = !DILocation(line: 271, column: 29, scope: !356)
!575 = !DILocation(line: 272, column: 29, scope: !356)
!576 = !DILocation(line: 273, column: 29, scope: !356)
!577 = !DILocation(line: 274, column: 29, scope: !356)
!578 = !DILocation(line: 275, column: 29, scope: !356)
!579 = !DILocation(line: 276, column: 29, scope: !356)
!580 = !DILocation(line: 277, column: 29, scope: !356)
!581 = !DILocation(line: 278, column: 29, scope: !356)
!582 = !DILocation(line: 279, column: 29, scope: !356)
!583 = !DILocation(line: 280, column: 29, scope: !356)
!584 = !DILocation(line: 268, column: 5, scope: !356)
!585 = !DILocation(line: 282, column: 11, scope: !356)
!586 = !DILocation(line: 282, column: 7, scope: !356)
!587 = !DILocation(line: 282, column: 16, scope: !356)
!588 = !DILocation(line: 282, column: 19, scope: !356)
!589 = !DILocation(line: 282, column: 17, scope: !356)
!590 = !DILocation(line: 282, column: 2, scope: !356)
!591 = !DILocation(line: 283, column: 42, scope: !356)
!592 = !DILocation(line: 283, column: 25, scope: !356)
!593 = !DILocation(line: 283, column: 15, scope: !356)
!594 = !DILocation(line: 283, column: 66, scope: !356)
!595 = !DILocation(line: 283, column: 58, scope: !356)
!596 = !DILocation(line: 283, column: 48, scope: !356)
!597 = !DILocation(line: 283, column: 45, scope: !356)
!598 = !DILocation(line: 283, column: 13, scope: !356)
!599 = !DILocation(line: 284, column: 59, scope: !356)
!600 = !DILocation(line: 284, column: 42, scope: !356)
!601 = !DILocation(line: 284, column: 71, scope: !356)
!602 = !DILocation(line: 284, column: 63, scope: !356)
!603 = !DILocation(line: 284, column: 13, scope: !356)
!604 = !DILocation(line: 285, column: 18, scope: !356)
!605 = !DILocation(line: 286, column: 9, scope: !356)
!606 = !DILocation(line: 287, column: 2, scope: !356)
!607 = !DILocation(line: 282, column: 30, scope: !356)
!608 = distinct !{!608, !590, !606, !302}
!609 = !DILocation(line: 288, column: 6, scope: !356)
!610 = !DILocation(line: 289, column: 3, scope: !356)
!611 = !DILocation(line: 290, column: 10, scope: !356)
!612 = !DILocation(line: 290, column: 15, scope: !356)
!613 = !DILocation(line: 290, column: 2, scope: !356)
!614 = !DILocation(line: 291, column: 1, scope: !356)
