	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0"
	.file	"add.c"
	.globl	main                            # -- Begin function main
	.p2align	1
	.type	main,@function
main:                                   # @main
# %bb.0:
	addi	sp, sp, -32
	sw	ra, 28(sp)                      # 4-byte Folded Spill
	sw	s0, 24(sp)                      # 4-byte Folded Spill
	addi	s0, sp, 32
	sw	zero, -12(s0)
	li	a0, 1
	sw	a0, -16(s0)
	li	a0, 2
	sw	a0, -20(s0)
	lw	a0, -16(s0)
	lw	a1, -20(s0)
	add	a0, a0, a1
	sw	a0, -24(s0)
	lw	a0, -24(s0)
	lw	ra, 28(sp)                      # 4-byte Folded Reload
	lw	s0, 24(sp)                      # 4-byte Folded Reload
	addi	sp, sp, 32
	ret
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
                                        # -- End function
	.ident	"clang version 15.0.0 (https://github.com/llvm/llvm-project.git 28b1ba1c0742a521037df7ef3a45cc969863eb06)"
	.section	".note.GNU-stack","",@progbits
