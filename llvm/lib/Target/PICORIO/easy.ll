; ModuleID = 'easy.c'
source_filename = "easy.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32-S128"
target triple = "riscv32"

; Function Attrs: noinline nounwind optnone
define dso_local i32 @main() #0 !dbg !8 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  store i32 0, ptr %1, align 4
  store i32 1, ptr %2, align 4, !dbg !11
  store i32 1, ptr %3, align 4, !dbg !12
  store i32 3, ptr %7, align 4, !dbg !13
  br label %13, !dbg !14

13:                                               ; preds = %43, %0
  %14 = load i32, ptr %7, align 4, !dbg !15
  %15 = icmp slt i32 %14, 1000, !dbg !16
  br i1 %15, label %16, label %46, !dbg !17

16:                                               ; preds = %13
  %17 = load i32, ptr %2, align 4, !dbg !18
  %18 = load i32, ptr %3, align 4, !dbg !19
  %19 = add nsw i32 %17, %18, !dbg !20
  store i32 %19, ptr %4, align 4, !dbg !21
  %20 = load i32, ptr %3, align 4, !dbg !22
  store i32 %20, ptr %2, align 4, !dbg !23
  %21 = load i32, ptr %4, align 4, !dbg !24
  store i32 %21, ptr %3, align 4, !dbg !25
  %22 = load i32, ptr %4, align 4, !dbg !26
  %23 = load i32, ptr %2, align 4, !dbg !27
  %24 = mul nsw i32 %22, %23, !dbg !28
  store i32 %24, ptr %3, align 4, !dbg !29
  %25 = load i32, ptr %3, align 4, !dbg !30
  %26 = load i32, ptr %2, align 4, !dbg !31
  %27 = add nsw i32 %25, %26, !dbg !32
  store i32 %27, ptr %9, align 4, !dbg !33
  %28 = load i32, ptr %9, align 4, !dbg !34
  %29 = load i32, ptr %3, align 4, !dbg !35
  %30 = mul nsw i32 %28, %29, !dbg !36
  store i32 %30, ptr %6, align 4, !dbg !37
  %31 = load i32, ptr %4, align 4, !dbg !38
  %32 = load i32, ptr %7, align 4, !dbg !39
  %33 = add nsw i32 %31, %32, !dbg !40
  store i32 %33, ptr %10, align 4, !dbg !41
  %34 = load i32, ptr %10, align 4, !dbg !42
  %35 = load i32, ptr %2, align 4, !dbg !43
  %36 = mul nsw i32 %34, %35, !dbg !44
  store i32 %36, ptr %11, align 4, !dbg !45
  %37 = load i32, ptr %11, align 4, !dbg !46
  %38 = load i32, ptr %10, align 4, !dbg !47
  %39 = mul nsw i32 %37, %38, !dbg !48
  store i32 %39, ptr %12, align 4, !dbg !49
  %40 = load i32, ptr %12, align 4, !dbg !50
  %41 = load i32, ptr %6, align 4, !dbg !51
  %42 = add nsw i32 %40, %41, !dbg !52
  store i32 %42, ptr %2, align 4, !dbg !53
  br label %43, !dbg !54

43:                                               ; preds = %16
  %44 = load i32, ptr %7, align 4, !dbg !55
  %45 = add nsw i32 %44, 1, !dbg !55
  store i32 %45, ptr %7, align 4, !dbg !55
  br label %13, !dbg !17, !llvm.loop !56

46:                                               ; preds = %13
  %47 = load i32, ptr %10, align 4, !dbg !58
  ret i32 %47, !dbg !59
}

attributes #0 = { noinline nounwind optnone "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+32bit,+a,+c,+m,+relax,-save-restore" }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!2, !3, !4, !5, !6}
!llvm.ident = !{!7}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 16.0.0 (https://github.com/llvm/llvm-project.git d3b95ecc98d204badbffa1840be7b7a06652a0a3)", isOptimized: false, runtimeVersion: 0, emissionKind: NoDebug, splitDebugInlining: false, nameTableKind: None)
!1 = !DIFile(filename: "easy.c", directory: "/work/stu/lizhuolun/llvm-project3.0/llvm-project/llvm/lib/Target/PICORIO")
!2 = !{i32 2, !"Debug Info Version", i32 3}
!3 = !{i32 1, !"wchar_size", i32 4}
!4 = !{i32 1, !"target-abi", !"ilp32"}
!5 = !{i32 7, !"frame-pointer", i32 2}
!6 = !{i32 1, !"SmallDataLimit", i32 8}
!7 = !{!"clang version 16.0.0 (https://github.com/llvm/llvm-project.git d3b95ecc98d204badbffa1840be7b7a06652a0a3)"}
!8 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 1, type: !9, scopeLine: 1, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !10)
!9 = !DISubroutineType(types: !10)
!10 = !{}
!11 = !DILocation(line: 2, column: 13, scope: !8)
!12 = !DILocation(line: 2, column: 21, scope: !8)
!13 = !DILocation(line: 4, column: 14, scope: !8)
!14 = !DILocation(line: 4, column: 13, scope: !8)
!15 = !DILocation(line: 4, column: 17, scope: !8)
!16 = !DILocation(line: 4, column: 18, scope: !8)
!17 = !DILocation(line: 4, column: 9, scope: !8)
!18 = !DILocation(line: 5, column: 21, scope: !8)
!19 = !DILocation(line: 5, column: 25, scope: !8)
!20 = !DILocation(line: 5, column: 23, scope: !8)
!21 = !DILocation(line: 5, column: 19, scope: !8)
!22 = !DILocation(line: 6, column: 21, scope: !8)
!23 = !DILocation(line: 6, column: 19, scope: !8)
!24 = !DILocation(line: 7, column: 21, scope: !8)
!25 = !DILocation(line: 7, column: 19, scope: !8)
!26 = !DILocation(line: 9, column: 21, scope: !8)
!27 = !DILocation(line: 9, column: 25, scope: !8)
!28 = !DILocation(line: 9, column: 23, scope: !8)
!29 = !DILocation(line: 9, column: 19, scope: !8)
!30 = !DILocation(line: 10, column: 21, scope: !8)
!31 = !DILocation(line: 10, column: 25, scope: !8)
!32 = !DILocation(line: 10, column: 23, scope: !8)
!33 = !DILocation(line: 10, column: 19, scope: !8)
!34 = !DILocation(line: 11, column: 21, scope: !8)
!35 = !DILocation(line: 11, column: 25, scope: !8)
!36 = !DILocation(line: 11, column: 23, scope: !8)
!37 = !DILocation(line: 11, column: 19, scope: !8)
!38 = !DILocation(line: 13, column: 21, scope: !8)
!39 = !DILocation(line: 13, column: 25, scope: !8)
!40 = !DILocation(line: 13, column: 23, scope: !8)
!41 = !DILocation(line: 13, column: 19, scope: !8)
!42 = !DILocation(line: 14, column: 22, scope: !8)
!43 = !DILocation(line: 14, column: 27, scope: !8)
!44 = !DILocation(line: 14, column: 25, scope: !8)
!45 = !DILocation(line: 14, column: 20, scope: !8)
!46 = !DILocation(line: 15, column: 22, scope: !8)
!47 = !DILocation(line: 15, column: 27, scope: !8)
!48 = !DILocation(line: 15, column: 25, scope: !8)
!49 = !DILocation(line: 15, column: 20, scope: !8)
!50 = !DILocation(line: 18, column: 21, scope: !8)
!51 = !DILocation(line: 18, column: 26, scope: !8)
!52 = !DILocation(line: 18, column: 24, scope: !8)
!53 = !DILocation(line: 18, column: 19, scope: !8)
!54 = !DILocation(line: 19, column: 9, scope: !8)
!55 = !DILocation(line: 4, column: 25, scope: !8)
!56 = distinct !{!56, !17, !54, !57}
!57 = !{!"llvm.loop.mustprogress"}
!58 = !DILocation(line: 21, column: 16, scope: !8)
!59 = !DILocation(line: 21, column: 9, scope: !8)
