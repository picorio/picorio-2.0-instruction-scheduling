; Test that the CPU names work.
;
; First ensure the error message matches what we expect.
; CHECK-ERROR: not a recognized processor for this target

; Now ensure the error message doesn't occur for valid CPUs.
; CHECK-NO ERROR-NOT : not a recognized processor for this target

;RUN: llc < %s -o /dev/null -mtriple=riscv64 -mcpu=c910 2>&1 | Filecheck %s --check-prefix=CHECK-NO-ERROR --allow-empty

define void @foo() {
    ret void
}
